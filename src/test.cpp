#include <rpany/rpany.hpp>
#include <rpany/tests.hpp>
#include "init.hpp"
#include "vmlstack.hpp"

using namespace rpany;

int main( int argc, char **argv )
{
	// Initialise the VML system
	vmlbatch::init( );

	// Create the suite
	vmlbatch::stack s;
	suite t( s, argv[ 0 ] );

	// Test empty stack first
	run_test( t, "start up", {
		RUN( "data stack is empty on startup", { }, { } ),
	} );

	// Add the default dictionaries
	educate( s );

	// Tests for default dictionaries
	run_test( t, "education", {
		RUN( "data stack is empty after dictionaries added", { }, { } ),
	} );

	// Create a test-iterate method
	s.evaluate( ": test-iterate length? 0 do i fetch drop loop ;" );

	eval_test( t, "test:", {
		EVAL( "accepted", "test:", { "test:" } ),
		EVAL( "sizeable", "test: length?", { "test:", 250 } ),
		EVAL( "no slots", "test: slots?", { "test:", 0 } ),
		EVAL( "fetchable", "test: 0 fetch", { "test:", "<frame>" } ),
		EVAL( "has image", "test: 0 fetch has-image?", { "test:", "<frame>", true } ),
		EVAL( "has alpha", "test: 0 fetch has-alpha?", { "test:", "<frame>", false } ),
		EVAL( "has audio", "test: 0 fetch has-audio?", { "test:", "<frame>", false } ),
		EVAL( "iterable", "test: test-iterate", { "test:" } ),
	} );

	eval_test( t, "tone:", {
		EVAL( "accepted", "tone:", { "tone:" } ),
		EVAL( "sizeable", "tone: length?", { "tone:", 250 } ),
		EVAL( "no slots", "tone: slots?", { "tone:", 0 } ),
		EVAL( "fetchable", "tone: 0 fetch", { "tone:", "<frame>" } ),
		EVAL( "has image", "tone: 0 fetch has-image?", { "tone:", "<frame>", false } ),
		EVAL( "has alpha", "tone: 0 fetch has-alpha?", { "tone:", "<frame>", false } ),
		EVAL( "has audio", "tone: 0 fetch has-audio?", { "tone:", "<frame>", true } ),
		EVAL( "iterable", "tone: test-iterate", { "tone:" } ),
	} );

	eval_test( t, "muxer", {
		EVAL( "muxer with no inputs fails with underflow", "filter:muxer", { }, e_underflow ),
		EVAL( "muxer with 1 input fails with underflow", "test: filter:muxer", { "test:" }, e_underflow ),
		EVAL( "muxer connects with test: and tone:", "test: tone: filter:muxer", { "muxer" } ),
	} );

	ml::input_type_ptr muxer;

	run_test( t, "null input_type_ptr", {
		RUN( "null input rejected", { muxer }, { }, e_std_exception ),
	} );

	s.evaluate( "test: tone: filter:muxer sync" ) >> muxer;

	run_test( t, "input_type_ptr", {
		RUN( "muxer from local var", { muxer }, { muxer } ),
		RUN( "muxer from local var as string", { muxer }, { "muxer" } ),
		RUN( "sizeable from local var", { muxer, "length?" }, { muxer, muxer->get_frames( ) } ),
		RUN( "slot count from local var", { muxer, "slots?" }, { muxer, muxer->slot_count( ) } ),
		RUN( "assign to variable", { muxer, "variable!", "muxer" }, { } ),
		RUN( "dereference variable", { "muxer", "@" }, { muxer } ),
	} );

	run_test( t, "decap", {
		RUN( "decap of local var", { muxer, "decap" }, { muxer->fetch_slot( 0 ), muxer->fetch_slot( 1 ) } ),
		RUN( "decap of input is a no op", { muxer, "decap", "decap" }, { muxer->fetch_slot( 0 ), muxer->fetch_slot( 1 ) } ),
	} );

	run_test( t, "playlist", {
		RUN( "playlist of 0 creates empty", { muxer, 0, "playlist", "slots?" }, { muxer, "playlist", 0 } ),
		RUN( "playlist of 1 underflows", { 1, "playlist", "slots?" }, { }, e_underflow ),
		RUN( "playlist of 1 connects", { muxer, 1, "playlist", "slots?" }, { "playlist", 1 } ),
		RUN( "playlist of 1 decaps", { muxer, 1, "playlist", "decap" }, { muxer } ),
		RUN( "playlist of 2 connects", { muxer, muxer, 2, "playlist", "slots?" }, { "playlist", 2 } ),
	} );

	eval_test( t, "variables as inputs", {
		EVAL( "fetchable", "muxer @ 0 fetch", { muxer, "<frame>" } ),
		EVAL( "has image", "muxer @ 0 fetch has-image?", { muxer, "<frame>", true } ),
		EVAL( "has alpha", "muxer @ 0 fetch has-alpha?", { muxer, "<frame>", false } ),
		EVAL( "has audio", "muxer @ 0 fetch has-audio?", { muxer, "<frame>", true } ),
		EVAL( "slot 0", "muxer @ 0 slot", { muxer, muxer->fetch_slot( 0 ) } ),
		EVAL( "slot 1", "muxer @ 1 slot", { muxer, muxer->fetch_slot( 1 ) } ),
		EVAL( "slot 2", "muxer @ 2 slot", { muxer }, e_stack ),
	} );

	eval_test( t, "variables in playlists", {
		EVAL( "playlist of 0 creates empty", "muxer @ 0 playlist slots?", { muxer, "playlist", 0 } ),
		EVAL( "playlist of 1 underflows", "1 playlist slots?", { }, e_underflow ),
		EVAL( "playlist of 1 connects", "muxer @ 1 playlist slots?", { "playlist", 1 } ),
		EVAL( "playlist of 1 decaps", "muxer @ 1 playlist decap", { muxer } ),
		EVAL( "playlist of 2 connects", "muxer @ test: 2 playlist slots?", { "playlist", 2 } ),
		EVAL( "playlist of 2 connects", "muxer @ test: 2 playlist decap", { muxer, "test:" } ),
		EVAL( "playlist of duplicated input", "muxer @ dup 2 playlist slots?", { "playlist", 2 } ),
	} );

	eval_test( t, "words", {
		EVAL( "test-graph is not defined", "test-graph", { }, e_undefined ),
		EVAL( "test-graph definition", ": test-graph test: tone: filter:muxer ;", { } ),
		EVAL( "test-graph is usable", "test-graph", { "muxer" } ),
		EVAL( "test-graph is correct size", "test-graph length?", { "muxer", 250 } ),
		EVAL( "test-graph reports 2 slots", "test-graph slots?", { "muxer", 2 } ),
		EVAL( "test-graph allows fetches", "test-graph 0 fetch", { "muxer", "<frame>" } ),
		EVAL( "test-graph allows iteration", "test-graph test-iterate", { "muxer" } ),
	} );

	eval_test( t, "property assignment", {
		EVAL( "input properties can be modified", "tone: out=5000 length?", { "tone:", 5000 } ),
		EVAL( "input properties can be modified after flush", "tone: out=5000 length? swap out=250 length?", { 5000, "tone:", 250 } ),
		EVAL( "test_graph rewrite to have more audio than video", ": test-graph test: tone: out=5000 filter:muxer ;", { } ),
		EVAL( "test-graph is correct size (longest input by default)", "test-graph length?", { "muxer", 5000 } ),
		EVAL( "test-graph can have behaviour changed", "test-graph use_longest=0 length?", { "muxer", 250 } ),
		EVAL( "attempt to assign an invalid property", "tone: foo=bar", { "tone:" }, e_stack ),
		EVAL( "attempt to conditionally assign an invalid property", "tone: ?foo=bar", { "tone:" } ),
		EVAL( "attempt to conditionally assign a valid property", "tone: ?foo=bar ?out=5000 length?", { "tone:", 5000 } ),
	} );

	eval_test( t, "property query", {
		EVAL( "prop-query throws", "prop-query", { }, e_stack ),
		EVAL( "tone: has an out property", "tone: $ out prop-exists", { "tone:", true } ),
		EVAL( "test: doesn't have an out property", "test: $ out prop-exists", { "test:", false } ),
		EVAL( "test: debug should be 0", "test: debug=0 $ debug prop-query", { "test:", 0 } ),
		EVAL( "test: debug should be 1", "test: debug=1 $ debug prop-query", { "test:", 1 } ),
		EVAL( "test: custom property", "test: @hello=there $ @hello prop-query", { "test:", "there" } ),
	} );

	return t.failed;
}
