#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <atomic>

namespace ml = olib::openmedialib::ml;
namespace pl = olib::openpluginlib;

namespace vmlbatch {

bool eof = false;

std::atomic< int > id_counter;

void exit_on_fail( bool success, const std::string &message, int error = 1 )
{
	if ( !success )
	{
		std::cerr << "ERROR: " << message << std::endl;
		exit( error );
	}
}

void shutdown( )
{
	ml::uninit( );
	pl::uninit( );
	ml::image::configure_global_cache( ml::image::image_pool_ptr( ) );
}

void init( int log_level )
{
	// Initialise the logging
	pl::init_log( );
	pl::set_log_level( log_level );

	// Initialise openpluginlib now
	exit_on_fail( pl::init( ), "Failed to initialise opl" );

	// Start up aml and register clean up on close
	exit_on_fail( ml::init( ), "Failed to initialise aml" );

	// Clean up on exit
	atexit( shutdown );
}

}
