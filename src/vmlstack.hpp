#pragma once

#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openmedialib/ml/io.hpp>
#include <rpany/rpany.hpp>
#include <boost/filesystem.hpp>
#include <stack>

namespace ml = olib::openmedialib::ml;
namespace pl = olib::openpluginlib;
namespace cl = olib::opencorelib;
namespace fs = boost::filesystem;

namespace vmlbatch {

struct bool_scope
{
	bool_scope( bool &var_ ) : var( var_ ) { }
	~bool_scope( ) { var = false; }
	bool &var;
};

struct input_scope
{
	input_scope( ml::input_type_ptr &var_ ) : var( var_ ) { }
	~input_scope( ) { var = ml::input_type_ptr( ); }
	ml::input_type_ptr &var;
};

enum accept_flags
{
	accept_none = 0x0,
	accept_input = 0x1,
	accept_filter = 0x2,
	accept_store = 0x4,
	accept_http_aml = accept_input | accept_filter,
	accept_all = accept_input | accept_filter | accept_store,
};

class stack : public rpany::stack
{
	public:
		stack( )
		{
			paths_.push( cl::str_util::to_string( fs::initial_path( ).native( ) ) );
		}

		stack( const rpany::stack &s )
		: rpany::stack( s )
		{ }

		stack( const rpany::stack &s, const rpany::dictionary &d )
		: rpany::stack( s, d )
		{ }

		void push( const rp::any &any )
		{
			handle( any );
		}

		void commit( )
		{
			handle( );
		}

		const rpany::word_list pending( ) const
		{
			if ( pending_ )
				return { pending_ };
			return { };
		}

		void purge_pending( )
		{
			pending_.reset( );
		}

		inline const accept_flags accepts( ) const
		{
			return accepts_;
		}

		void set_accepts( accept_flags accepts )
		{
			accepts_ = accepts;
		}

	protected:
		bool is_extended( ) const
		{
			return true;
		}

	private:
		void handle( const rp::any &any )
		{
			const auto &s = rpany::if_string( any );
			if ( trace_ & 1 ) std::cerr << "! " << s << std::endl;
			if ( rpany::is< rpany::parser_ptr >( any ) )
				rpany::stack::push( any );
			else if ( is_parsing( ) || rpany::is_numeric( any ) || is_word( s ) )
				rpany::stack::push( any );
			else if ( rpany::is< ml::io::layer_ptr >( any ) )
				rpany::stack::push( any );
			else if ( is_store( any, s ) )
				handle_store( any, s );
			else if ( is_filter( any, s ) )
				handle_filter( any, s );
			else if ( is_property( any, s ) )
				handle_property( any, s );
			else if ( is_input( any, s ) )
				handle_input( any, s );
			else
				rpany::stack::push( any );
		}

		bool is_store( const rp::any &any, const std::string &s ) const
		{
			if ( rpany::is< ml::store_type_ptr >( any ) ) return true;
			return s.find( "store:" ) == 0;
		}

		bool is_filter( const rp::any &any, const std::string &s ) const
		{
			if ( rpany::is< ml::filter_type_ptr >( any ) ) return true;
			return s.find( "filter:" ) == 0;
		}

		bool is_property( const rp::any &any, const std::string &s ) const
		{
			const size_t equal = s.find( "=" );
			const size_t colon = s.find( ":" );
			const size_t brace = s.find( "[" );
			return s != "" && equal != std::string::npos && ( s[ 0 ] == '@' || ( brace > equal && colon > equal ) );
		}

		bool is_input( const rp::any &any, const std::string &s ) const
		{
			if ( rpany::is< ml::input_type_ptr >( any ) ) return true;
			else if ( s.back( ) == ':' ) return true;
			else if ( is_url( s ) ) return true;
			else return std::ifstream( expand_path( s ) ).good( );
		}

		bool is_url( const std::string &s ) const
		{
			auto colon = s.find( ":" );
			return colon != std::string::npos && colon != 0;
		}

		bool is_aml( const std::string &s ) const
		{
			return s.rfind( ".aml" ) == s.length( ) - 4;
		}

		bool is_aml_http( const std::string &s ) const
		{
			return ( s.find( "http://" ) == 0 || s.find( "https://" ) == 0 ) && is_aml( s );
		}

		ml::input_type_ptr load_aml_http( const std::string &url )
		{
			if ( ( accepts( ) & accept_http_aml ) != accept_http_aml )
				RPANY_THROW( rpany::security_exception, "Not authorised to load " + url );

			ml::io::request request( url, ml::io::access_mode::read );
			ml::io::layer_ptr file = request.layer( );

			ARENFORCE_MSG( !!file, "Unable to access %s" )( url );
			ARENFORCE_MSG( file->open( ) == 0, "Unable to open %s" )( url );

			unsigned char buffer[ 16384 ];
			std::ostringstream contents;
			int error = 0;

			while( error == 0 )
			{
				boost::uint32_t result_size = 0;
				error = file->read( buffer, sizeof( buffer ) - 1, result_size );
				if ( error == 0 || result_size > 0 )
				{
					buffer[ result_size ] = '\0';
					contents << buffer;
				}
			}

			ARENFORCE_MSG( file->close( ) == 0, "Unable to close %s" )( url );

			std::istringstream input( contents.str( ) );
			stack eval_stack;
			eval_stack.set_accepts( accept_http_aml );

			paths_.push( url.substr( 0, url.rfind( '/' ) ) );
			try
			{
				rpany::eval( eval_stack, input, true );
			}
			catch( ... )
			{
				paths_.pop( );
				throw;
			}

			if ( eval_stack.depth( ) == 0 )
				RPANY_THROW( rpany::stack_exception, "No graph obtained when parsing " + url );
			if ( eval_stack.depth( ) > 1 )
				RPANY_THROW( rpany::stack_exception, "Multiple graphs obtained when parsing " + url );

			return eval_stack.pull< ml::input_type_ptr >( );
		}

		ml::input_type_ptr load_aml_file( const std::string &filename )
		{
			if ( ( accepts( ) & accept_http_aml ) != accept_http_aml )
				RPANY_THROW( rpany::security_exception, "Not authorised to load " + filename );

			std::ifstream stream( filename );
			if ( !stream.good( ) ) RPANY_THROW( rpany::stack_exception, "Unable to load '" + filename + "'" );

			paths_.push( cl::str_util::to_string( fs::system_complete( filename ).parent_path( ).native( ) ) );

			try
			{
				rpany::eval( self( ), stream, true );
			}
			catch( ... )
			{
				paths_.pop( );
				throw;
			}

			return pull< ml::input_type_ptr >( );
		}

		void handle_store( const rp::any &any, const std::string &s )
		{
			if ( !( accepts( ) & accept_store ) )
				RPANY_THROW( rpany::security_exception, "Not authorised to create store " + s );

			commit( );
			if ( rpany::is< ml::store_type_ptr >( any ) ) return;
			pending_ = ml::create_filter( "store" );
			if ( !pending_ ) RPANY_THROW( rpany::stack_exception, "Unable to create filter:store" );
			pl::pcos::property_container props = pending_->properties( );
			if ( s.substr( 6 ) != "sdl_video:" )
				assign( props, "store=" + s.substr( 6 ) );
			else
				assign( props, "store=vmlbatch-video:" );
		}

		const std::string expand_path( const std::string &s ) const
		{
			std::string result = rpany::unquote( s );
			auto colon = result.rfind( ':' );

			// We don't want to interfere with fully expressed uri's here
			if ( result == "" ||
				 colon == result.length( ) - 1 ||
				 result.find( "http://" ) != std::string::npos ||
				 result.find( "https://" ) != std::string::npos ||
				 result.find( "ftp://" ) != std::string::npos )
				return result;

#ifdef _WIN32
			// Attempt to handle windows annoyances
			if ( colon == 1 )
				colon = std::string::npos;
			else if ( colon > 1 && result[ colon - 2 ] == ':' )
				colon -= 2;
#endif

			const auto path = paths_.top( );

			if ( path.find( "http://" ) == 0 || path.find( "https://" ) == 0 )
			{
				result = path + "/" + result;
			}
			else if ( colon != std::string::npos )
			{
				if ( fs::path( result.substr( colon + 1 ) ).is_relative( ) )
				{
					auto full = fs::path( path ) / result.substr( colon + 1 );
					if ( fs::exists( full ) )
						result = result.substr( 0, colon + 1 ) + cl::str_util::to_string( full.native( ) );
				}
			}
			else
			{
				if ( fs::path( result ).is_relative( ) )
				{
					auto full = fs::path( path ) / result;
					if ( fs::exists( full ) )
						result = cl::str_util::to_string( full.native( ) );
				}
			}

			return result;
		}

		void handle_input( const rp::any &any, const std::string &s )
		{
			if ( !( accepts( ) & accept_input ) )
				RPANY_THROW( rpany::security_exception, "Not authorised to create input " + s );

			commit( );
			const std::string path = expand_path( s );
			if ( rpany::is< ml::input_type_ptr >( any ) )
				pending_ = rpany::as< ml::input_type_ptr >( any );
			else if ( is_aml_http( path ) )
				pending_ = load_aml_http( path );
			else if ( is_aml( path ) )
				pending_ = load_aml_file( path );
			else
				pending_ = ml::create_delayed_input( path );
			if ( !pending_ ) RPANY_THROW( rpany::stack_exception, "Invalid input '" + s + "'" );
		}

		void handle_filter( const rp::any &any, const std::string &s )
		{
			if ( !( accepts( ) & accept_store ) && s == "filter:store" )
				RPANY_THROW( rpany::security_exception, "Not authorised to create " + s );
			if ( !( accepts( ) & accept_filter ) )
				RPANY_THROW( rpany::security_exception, "Not authorised to create " + s );

			commit( );
			if ( rpany::is< ml::filter_type_ptr >( any ) )
				pending_ = rpany::as< ml::filter_type_ptr >( any );
			else
				pending_ = ml::create_filter( s.substr( 7 ) );
			if ( !pending_ ) RPANY_THROW( rpany::stack_exception, "Invalid filter '" + s + "'" );
		}

		void handle_property( const rp::any &any, const std::string &s )
		{
			if ( accepts( ) == accept_none )
				RPANY_THROW( rpany::security_exception, "Not authorised to assign property " + s );

			// We want to ensure we don't allow a flush now
			bool_scope closing( closing_ = true );

			// If we weren't pending before, we are now
			if ( !pending_ )
				pending_ = pull< ml::input_type_ptr >( );
			if ( !pending_ )
				RPANY_THROW( rpany::stack_exception, "No filter or graph for '" + s + "'" );

			pl::pcos::property_container props = pending_->properties( );
			assign( props, s, pending_->get_uri( ) == L"store" );
		}

	public:
		void assign( pl::pcos::property_container &props, const std::string &token, const std::string &value, bool is_store_filter = false )
		{
			std::string name = token;
			const bool ignore_failure = token[ 0 ] == '?';
			if ( ignore_failure ) name = name.substr( 1 );

			pl::pcos::key key = pl::pcos::key::from_string( name.c_str( ) );
			pl::pcos::property property = props.get_property_with_key( key );

			if ( property.valid( ) )
			{
				property.set_from_string( cl::str_util::to_wstring( value ) );
			}
			else if ( is_store_filter && name.find( "@^" ) == 0 )
			{
				pl::pcos::key store_key = pl::pcos::key::from_string( ( name.substr( 2 ) ).c_str( ) );
				property = props.get_property_with_key( store_key );
				if ( property.valid( ) )
					property.set_from_string( cl::str_util::to_wstring( value ) );
				else
					RPANY_THROW( rpany::stack_exception, "Invalid property '" + name + "'" );
			}
			else if ( is_store_filter && name.find( "@store." ) != 0 )
			{
				pl::pcos::key store_key = pl::pcos::key::from_string( ( "@store." + name ).c_str( ) );
				pl::pcos::property prop( store_key );
				props.append( prop = cl::str_util::to_wstring( value ) );
			}
			else if ( name[ 0 ] == '@' )
			{
				pl::pcos::property prop( key );
				props.append( prop = cl::str_util::to_wstring( value ) );
			}
			else if ( !ignore_failure )
			{
				RPANY_THROW( rpany::stack_exception, "Invalid property '" + name + "'" );
			}
		}

		void assign( pl::pcos::property_container &props, const std::string &pair, bool is_store_filter = false )
		{
			size_t pos = pair.find( "=" );

			std::string name = pair.substr( 0, pos );
			std::string value = pair.substr( pos + 1 );

			if ( !value.empty( ) && value[ 0 ] == '"' && value[ value.size( ) - 1 ] == '"' )
				value = value.substr( 1, value.size( ) - 2 );

			if ( value == "%s" )
			{
				value = coerce< std::string >( );
				pop( );
			}

			assign( props, name, value, is_store_filter );
		}

	private:
		void handle( )
		{
			// Return immediately if already closing or nothing pending
			if ( closing_ || !pending_ ) return;

			// Ensure we clean up the closing scope when done
			bool_scope closing( closing_ = true );
			input_scope input( pending_ );

			// Initialise the input
			if ( pending_->slot_count( ) == 0 )
				pending_->init( );

			// Count number of items required
			int count = 0;
			for ( size_t i = 0; i < pending_->slot_count( ); i ++ )
				if ( !pending_->fetch_slot( i ) )
					count ++;

			// Connect them
			for( int i = count, j = 0; i > 0; j ++ )
			{
				if ( pending_->fetch_slot( j ) ) continue;

				auto &any = pick( -- i );

				// If the item isn't an input, coerce it now
				if ( !rpany::is< ml::input_type_ptr >( any ) )
				{
					auto input = coerce< ml::input_type_ptr >( i );
					input->init( );
					any = input;
				}

				pending_->connect( rpany::as< ml::input_type_ptr >( any ), j );
			}

			// Remove consumed inputs
			pop( count );

			// Append pending to the stack
			append( pending_ );
		}

		accept_flags accepts_ = accept_none;
		bool closing_ = false;
		ml::input_type_ptr pending_;
		std::stack< std::string > paths_;
};

}
