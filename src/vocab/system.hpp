#pragma once

RPANY_OPEN( debug, rpany::stack &s )
	s.teach( "log-level", [] ( rpany::stack &s ) { pl::set_log_level( s.pull< int >( ) ); } );
RPANY_CLOSE( debug );

RPANY_OPEN( server, rpany::stack &s )
	s.evaluate( ": server-factory ss-new ss-educate-all [[ study shell.rp ]] ss-eval-list ;" );
RPANY_CLOSE( server );

RPANY_OPEN( os, rpany::stack &s )
	if ( &s.input( ) == &std::cin && rpany::is_tty( ) )
	{
		s.teach( "system", [] ( rpany::stack &s ) { s.push( system( s.pull< std::string >( ).c_str( ) ) ); } );
		s.evaluate( ": sh inline parse [[ system drop ]] ;" );
	}
RPANY_CLOSE( os );
