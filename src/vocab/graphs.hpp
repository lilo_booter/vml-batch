#pragma once

#ifdef _MSC_VER
#define strncasecmp _strnicmp
#define strcasecmp _stricmp
#endif

inline void length( rpany::stack &stack )
{
	stack.cast< ml::input_type_ptr >( )->sync( );
	stack += stack.cast< ml::input_type_ptr >( )->get_frames( );
}

inline void slots( rpany::stack &stack )
{
	stack += stack.cast< ml::input_type_ptr >( )->slot_count( );
}

inline void slot( rpany::stack &stack )
{
	auto index = stack.pull< size_t >( );
	auto input = stack.cast< ml::input_type_ptr >( );
	if ( index >= input-> slot_count( ) ) RPANY_THROW( rpany::stack_exception, "Index out of range" );
	stack += input->fetch_slot( index );
}

inline void connect( rpany::stack &stack )
{
	auto index = stack.pull< size_t >( );
	auto slot = stack.pull< ml::input_type_ptr >( );
	auto input = stack.cast< ml::input_type_ptr >( );
	if ( index >= input-> slot_count( ) ) RPANY_THROW( rpany::stack_exception, "Index out of range" );
	input->connect( slot, index );
}

inline void reconnect( rpany::stack &stack )
{
	auto input = stack.cast< ml::input_type_ptr >( );
	for ( size_t index = 0; index < input->slot_count( ); index ++ )
		input->connect( input->fetch_slot( index ), index );
	input->sync( );
}

inline void decap( rpany::stack &stack )
{
	auto input = stack.pull< ml::input_type_ptr >( );
	if ( input->slot_count( ) )
		for ( size_t index = 0; index < input->slot_count( ); index ++ )
			stack += input->fetch_slot( index );
	else
		stack += input;
}

size_t get_max_width( rpany::stack &s )
{
	size_t result = 132;
	if ( s.is_word( std::string( "report-max-width" ) ) )
		s << "report-max-width" >> result;
	return result;
}

bool compare( pl::pcos::key a, pl::pcos::key b )
{
	return strcasecmp( a.as_string( ), b.as_string( ) ) < 0;
}

void report_props( std::ostream &os, const std::string &label, const pl::pcos::property_container &props, size_t width )
{
	// Obtain the keys on the filter
	pl::pcos::key_vector keys = props.get_keys( );

	if ( keys.size( ) )
	{
		auto current_size = label.size( );
		size_t count = 0;

		os << label;

		// For each key...
		std::sort( keys.begin( ), keys.end( ), compare );
		for( pl::pcos::key_vector::iterator it = keys.begin( ); it != keys.end( ); ++it )
		{
			std::ostringstream tmp;
			tmp << ( *it ).as_string( ) << "=" << props.get_property_with_key( *it );
			if ( count > 0 && current_size + tmp.str( ).size( ) >= width )
			{
				os << std::endl << std::string( label.size( ), ' ' );
				count = 0;
				current_size = label.size( );
			}
			os << tmp.str( ) << " ";
			count ++;
			current_size += tmp.str( ).size( ) + 1;
		}
		os << std::endl;
	}
}

void describe_frame( rpany::stack &stack )
{
	auto &out = stack.output( );
	auto f = stack.cast< vmlbatch_frame_ptr >( );

	const auto &video = f->get_video_packet( );
	size_t max_width = get_max_width( stack );

	out
	<< "Position   : "
	<< f->get_position( ) << " at "
	<< f->get_fps_num( ) << ":" << f->get_fps_den( ) << " fps "
	<< std::endl;
	if ( f->has_image( ) )
		out
		<< "Image      : "
		<< f->width( ) << "x" << f->height( ) << "@" << f->get_sar_num( ) << ":" << f->get_sar_den( ) << " as "
		<< cl::str_util::to_string( f->pf( ) ) << " "
		<< std::endl;
	if ( f->get_alpha( ) )
		out
		<< "Alpha      : "
		<< f->get_alpha( )->width( ) << "x" << f->get_alpha( )->height( ) << "@" << f->get_sar_num( ) << ":" << f->get_sar_den( ) << " as "
		<< cl::str_util::to_string( f->get_alpha( )->pf( ) ) << " "
		<< std::endl;
	if ( f->has_audio( ) )
		out
		<< "Audio      : "
		<< f->samples( ) << " samples at "
		<< f->frequency( ) << " hz in "
		<< f->channels( ) << " channels as "
		<< cl::str_util::to_string( f->get_audio( )->af( ) ) << " "
		<< std::endl;
	if ( video )
		out
		<< "Video      : "
		<< video->codec( ) << " "
		<< ( video->is_key_frame( ) ? "key-frame " : "" )
		<< "dts=" << video->dts_offset( ) << " "
		<< "pts=" << video->pts_offset( ) << " "
		<< std::endl;
	report_props( out, "Properties : ", f->properties( ), max_width );
	if ( f->get_video_packet( ) )
		report_props( out, "Video Props: ", f->get_video_packet( )->properties( ), max_width );
}

void describe_frame_at( rpany::stack &stack )
{
	stack << "fetch" << "describe-frame" << "drop";
}

inline void describe( rpany::stack &stack )
{
	auto &out = stack.output( );
	auto input = stack.pick( );

	if ( rpany::is< ml::input_type_ptr >( input ) )
	{
		ml::filter_type_ptr aml = ml::create_filter( "aml" );
		stack << aml << "filename=@" << "decap";
		out << aml->property( "stdout" ).value< std::string >( );
	}
	else
	{
		out << rpany::convert< std::string >( input ) << std::endl;
	}
}

inline void commit( rpany::stack &stack )
{
	stack.commit( );
}

inline void purge_pending( rpany::stack &stack )
{
	dynamic_cast< vmlbatch::stack & >( stack ).purge_pending( );
}

inline void see_pending( rpany::stack &stack )
{
	const auto pending = stack.pending( );
	auto &stream = stack.output( );
	for( auto iter = pending.begin( ); iter != pending.end( ); iter ++ )
	{
		ml::input_type_ptr input = rpany::as< ml::input_type_ptr >( *iter );
		if ( input->slot_count( ) >= 1 ) stream << "filter:";
		stream << cl::str_util::to_string( input->get_uri( ) ) << " " << input->properties( ) << std::endl;
	}
}

inline void seekable( rpany::stack &stack )
{
	stack += stack.cast< ml::input_type_ptr >( )->is_seekable( );
}

inline void complete( rpany::stack &stack )
{
	stack += stack.cast< ml::input_type_ptr >( )->is_complete( );
}

inline void distributable( rpany::stack &stack )
{
	stack += static_cast< int >( stack.cast< ml::input_type_ptr >( )->is_distributable( ) );
}

inline bool analyse_distribution( const ml::input_type_ptr &input )
{
	if ( !input ) return false;

	bool result = false;

	switch( input->is_distributable( ) )
	{
		case ml::distributable_no:
			break;
		case ml::distributable_should:
			result = true;
			break;
		case ml::distributable_could:
			for ( size_t index = 0; !result && index < input->slot_count( ); index ++ )
				result = analyse_distribution( input->fetch_slot( index ) );
			break;
	}

	return result;
}

inline void graph_distributable( rpany::stack &stack )
{
	stack += analyse_distribution( stack.cast< ml::input_type_ptr >( ) );
}

inline void sync( rpany::stack &stack )
{
	stack.cast< ml::input_type_ptr >( )->sync( );
}

// Coercion rules for string to input

inline rp::any string_to_input( const rp::any &any )
{
	const std::string &string = rpany::as< std::string >( any );
	auto input = ml::create_delayed_input( rpany::unquote( string ) );
	if ( !input ) RPANY_THROW( rpany::stack_exception, "Invalid input '" + string + "'" );
	return input;
}

// Coercion rules for input to string

inline rp::any input_to_string( const rp::any &any )
{
	const auto input = rpany::as< ml::input_type_ptr >( any );
	return input ? cl::str_util::to_string( input->get_uri( ) ) : std::string( "<undefined-input>" );
}

// Annoyingly, stores don't even provide a method which indicates how they were constructed

inline rp::any store_to_string( const rp::any &any )
{
	return std::string( "<store>" );
}

// Expects a smart_pusher: started graph as input at top of stack

inline void create_puller( rpany::stack &stack )
{
	auto input = stack.pull< ml::input_type_ptr >( );
	auto output = ml::create_store( L"smart_puller:", ml::frame_type_ptr( ) );
	if ( !output )
		RPANY_THROW( rpany::stack_exception, "Unable to create smart_puller" );
	output->property( "graph" ) = input;
	output->init( );
	stack += output;
}

// Expects <store> <input> <frame> at top of stack

inline void store_push( rpany::stack &stack )
{
	auto frame = stack.pull< vmlbatch_frame_ptr >( );
	auto input = stack.pull< ml::input_type_ptr >( );
	auto output = stack.cast< ml::store_type_ptr >( );
	stack += input;
	output->push( std::move( frame->shallow( ) ) );
}

// Expects <store> at top of stack

inline void store_complete( rpany::stack &stack )
{
	auto output = stack.pull< ml::store_type_ptr >( );
	output->complete( );
}

RPANY_OPEN( graphs, rpany::stack &s )
	dynamic_cast< vmlbatch::stack * >( &s )->set_accepts( vmlbatch::accept_all );
	s.teach( "length?", length );
	s.teach( "slots?", slots );
	s.teach( "seekable?", seekable );
	s.teach( "complete?", complete );
	s.teach( "distributable?", distributable );
	s.teach( "distributable-graph?", graph_distributable );
	s.teach( "sync", sync );
	s.teach( "slot", slot );
	s.teach( "connect", connect );
	s.teach( "reconnect", reconnect );
	s.teach( "decap", decap );
	s.teach( "describe", describe );
	s.teach( "describe-frame", describe_frame );
	s.teach( "describe-frame-at", describe_frame_at );
	s.teach( "commit", commit );
	s.teach( "purge-pending", purge_pending );
	s.teach( "see-pending", see_pending );
	s.evaluate( ": playlist filter:playlist slots=%s commit ;" );
	s.evaluate( ": clone filter:aml filename=@ $ stdout prop-query swap decap swap eval commit ;" );
	rpany::coercion( COERCE_KEY( ml::store_type_ptr, std::string ), store_to_string );
	rpany::coercion( COERCE_KEY( ml::input_type_ptr, std::string ), input_to_string );
	rpany::coercion( COERCE_KEY( std::string, ml::input_type_ptr ), string_to_input );
RPANY_CLOSE( graphs );

RPANY_OPEN( stores, rpany::stack &s )
	s.teach( "create-puller", create_puller );
	s.teach( "store-push", store_push );
	s.teach( "store-complete", store_complete );
RPANY_CLOSE( stores );

RPANY_OPEN( smart, rpany::stack &s )
	s.evaluate( ": smart-settings ?image=720p ?audio=48000:2 ?fps=50 ;" );
	s.evaluate( ": join filter:smart_playlist smart-settings slots=2 commit ;" );
	s.evaluate( ": side-by-side filter:smart_mosaic smart-settings slots=2 commit ;" );
	s.evaluate( ": over-and-under filter:smart_mosaic smart-settings arrangement=vgrid slots=2 commit ;" );
	s.evaluate( ": smart-playlist filter:smart_playlist smart-settings slots=%s commit ;" );
RPANY_CLOSE( smart );
