#pragma once

inline pl::pcos::property_container props_get( rp::any &tos )
{
	using namespace rpany;
	if ( is< ml::input_type_ptr >( tos ) )
		return as< ml::input_type_ptr >( tos )->properties( );
	else if ( is< ml::store_type_ptr >( tos ) )
		return as< ml::store_type_ptr >( tos )->properties( );
	else if ( is< ml::video_packet_type_ptr >( tos ) )
		return as< ml::video_packet_type_ptr >( tos )->properties( );
	else if ( is< vmlbatch_frame_ptr >( tos ) )
		return as< vmlbatch_frame_ptr >( tos )->properties( );
	else
		RPANY_THROW( stack_exception, "Unable to locate properties on '" + convert< std::string >( tos ) + "'" );
}

inline void prop_see( rpany::stack &stack )
{
	report_props( stack.output( ), "", props_get( stack.pick( ) ), get_max_width( stack ) );
}

inline std::string prop_value( const pl::pcos::property &prop )
{
	std::ostringstream stream;
	stream << prop;
	return rpany::unquote( stream.str( ) );
}

inline std::string prop_value( const pl::pcos::property_container &props, const std::string &key )
{
	return prop_value( props.get_property_with_string( key.c_str( ) ) );
}

inline bool prop_exists( const pl::pcos::property_container &props, const std::string &key )
{
	return props.get_property_with_string( key.c_str( ) ).valid( );
}

inline void prop_exists( rpany::stack &stack )
{
	auto key = stack.pull< std::string >( );
	pl::pcos::property_container props = props_get( stack.pick( ) );
	stack += props.get_property_with_string( key.c_str( ) ).valid( );
}

inline void prop_query( rpany::stack &stack )
{
	auto key = stack.pull< std::string >( );
	pl::pcos::property_container props = props_get( stack.pick( ) );
	stack += rpany::unquote( prop_value( props, key ) );
}

inline void prop_report( rpany::stack &stack, bool slots )
{
	auto key = stack.pull< std::string >( );
	if ( key.find( "filter:" ) == 0 )
	{
		auto filter = ml::create_filter( key.substr( 7 ) );
		if ( filter == 0 )
			RPANY_THROW( rpany::stack_exception, "Unable to create " + key );
		report_props( stack.output( ), "", filter->properties( ), 0 );
		if ( slots )
			stack.output( ) << "_slots_=" << filter->slot_count( ) << std::endl;
	}
	else if ( key.find( "input:" ) == 0 )
	{
		ml::input_type_ptr input;
		try { input = ml::create_delayed_input( key.substr( 6 ) ); }
		catch( ... ) { }
		if ( input == 0 )
			try { input = ml::create_delayed_input( key.substr( 6 ) + "file:" ); }
			catch( ... ) { }
		if ( input == 0 )
			RPANY_THROW( rpany::stack_exception, "Unable to create " + key );
		report_props( stack.output( ), "", input->properties( ), 0 );
		if ( slots )
			stack.output( ) << "_slots_=0" << std::endl;
	}
	else
	{
		RPANY_THROW( rpany::stack_exception, "Only filters and inputs are currently supported here" );
	}
}

inline void prop_report( rpany::stack &stack )
{
	prop_report( stack, false );
}

inline void prop_report_slots( rpany::stack &stack )
{
	prop_report( stack, true );
}

inline void prop_list( rpany::stack &stack )
{
	rp::any tos = stack.pick( );
	if ( rpany::is_numeric( tos ) )
		stack << "list-to-vector";

	auto vector = stack.pull< rpany::vector_ptr >( );
	auto props = props_get( stack.pick( ) );
	for ( auto iter : *vector )
	{
		const auto key = rpany::as< std::string >( iter );
		if ( key[ 0 ] == '?' )
			stack += prop_exists( props, key.substr( 1 ) ) ? prop_value( props, key.substr( 1 ) ) : "";
		else if ( prop_exists( props, key ) )
			stack += prop_value( props, key );
		else
			RPANY_THROW( rpany::stack_exception, "Unable to find a property called " + key );
	}
	stack += vector->size( );
}

RPANY_OPEN( properties, rpany::stack &s )
	s.teach( "prop-see", prop_see );
	s.teach( "prop-exists", prop_exists );
	s.teach( "prop-query", prop_query );
	s.teach( "prop-report", prop_report );
	s.teach( "prop-report-slots", prop_report_slots );
	s.teach( "prop-list", prop_list );
RPANY_CLOSE( properties );
