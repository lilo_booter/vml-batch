#pragma once

void assign_variable( rpany::stack &s, const std::string &name )
{
	if ( name == "" )
		RPANY_THROW( rpany::undefined_exception, "Invalid token for local variable name" );
	s.validate( name );
	auto &frame = s.get_stack_frame( );
	frame[ name ] = s.pick( );
}


void use_variable( rpany::stack &s, const std::string &name )
{
	rp::any local;
	if ( !s.is_local_var( name, local ) )
		RPANY_THROW( rpany::undefined_exception, "No variable called '" + name + "' found" );
	s += local;
}

void wipe_variable( rpany::stack &s, const std::string &name )
{
	auto &frame = s.get_stack_frame( );
	auto iter = frame.find( name );
	if ( iter == frame.end( ) )
		RPANY_THROW( rpany::undefined_exception, "No variable called '" + name + "' found" );
	frame.erase( iter );
}

RPANY_OPEN( aml_vars, rpany::stack &s )
	s.teach( "assign_variable", rpany::create_parse_token< assign_variable > );
	s.teach( "use_variable", rpany::create_parse_token< use_variable > );
	s.teach( "wipe_variable", rpany::create_parse_token< wipe_variable > );
RPANY_CLOSE( aml_vars );
