#pragma once

typedef std::vector< rp::any > vector_type;
typedef std::shared_ptr< vector_type > vector_ptr;

enum flags
{
	allow_none = 0x00,
	allow_trick_play = 0x01,
	allow_pitch = 0x02,
	allow_pause = 0x04,
	allow_all = allow_trick_play | allow_pitch | allow_pause,
};

#define allows_trick_play( o ) ( o & allow_trick_play )
#define allows_pitch( o )	   ( o & allow_pitch )
#define allows_pause( o )	   ( o & allow_pause )

inline bool toggle_fullscreen( ml::store_type_ptr &store, bool is_full )
{
	pl::pcos::property fullscreen = store->properties( ).get_property_with_string( "fullscreen" );

	if( !fullscreen.valid( ) )
	{
		pl::pcos::property video_store_prop = store->properties().get_property_with_string( "video_store" );

		if( video_store_prop.valid( ) )
		{
			ml::store_type_ptr video_store = video_store_prop.value< ml::store_type_ptr >( );
			fullscreen = video_store->properties( ).get_property_with_string( "fullscreen" );
		}
	}

	if( fullscreen.valid( ) )
		fullscreen = is_full ? 0 : 1;

	return !is_full;
}

inline void set_grab_audio( ml::store_type_ptr &store )
{
	pl::pcos::property grab_audio = store->properties( ).get_property_with_string( "grab_audio" );

	if( grab_audio.valid( ) )
	{
		grab_audio.set( 1 );
		grab_audio.update( );
	}
}

template< typename T >
T obtain_var( rpany::stack &stack, const std::string &name, const T &def )
{
	T result = def;
	if ( stack.vfind( name ) != stack.vend( ) )
		stack << name << "@" >> result;
	return result;
}

enum stats_type
{
	stats_per_line = -1,
	stats_off = 0,
	stats_on = 1,
};

class player_t
{
	public:
		player_t( rpany::stack_ptr stack_ )
		: stack_ptr( stack_ )
		, stack( *stack_ )
		{ }

		~player_t( )
		{
			try
			{
				stores_complete( );
			}
			catch( ... )
			{
				std::cerr << "FATAL: Errors thrown while completing stores" << std::endl;
			}
		}

		template< typename T >
		T obtain_stack_var( const std::string &type, const std::string &name, const T &def )
		{
			return obtain_var< T >( stack, type + "-" + name, def );
		}

		void play( const std::string &type, stats_type stats = stats_off )
		{
			// Turn on stats if requested
			if ( ml::create_filter( "smart_stats" ) )
				stack
				<< "filter:smart_stats"
				<< ( boost::format( "flags=%d" ) % obtain_stack_var( type, "flags", int( 0xff ) ) ).str( )
				;

			// Fetch input from stack
			if ( !input ) input = normalise( type );

			// If we have no input now, we have to stop
			if ( !input ) RPANY_THROW( rpany::stack_exception, "No input accessible - job stopping" );

			// We'll serialise now so that we can enquire on the full graph from the shell/script
			serialise( input );

			// Honour other requests if applicable
			set_position( input->get_position( ) );
			if ( input->is_seekable( ) )
				set_position( obtain_stack_var( type, "position", input->get_position( ) ) );

			set_speed( 1 );
			if ( allows_trick_play( options ) )
				set_speed( obtain_stack_var( type, "speed", 1 ) );

			set_pitch( 1.0 );
			if ( allows_pitch( options ) )
				set_pitch( obtain_stack_var( type, "pitch", 1.0 ) );

			if ( pitch_filter )
				pitch_filter->properties( ).get_property_with_string( "speed" ) = pitch( );

			// Sanity check that we can get a frame
			ml::frame_type_ptr frame;
			ml::fetch_status status = input->fetch( frame, position( ) );
			if ( status != ml::fetch_ok || frame == 0 || frame->in_error( ) ) RPANY_THROW( rpany::stack_exception, "Unable to obtain frame" );

			// Create stores now
			if ( should_create_stores )
				stores_create( type, frame );
			else
				set_position( position( ) + 1 );

			// Attempt to detect playlist and related slots
			if ( allows_trick_play( options ) )
				analyse_playlist( input );

			bool incomplete = !input->is_complete( );
			int frames_since_sync = 0;

			while( !is_stopped( ) && running && !is_interrupted( type ) )
			{
				if ( should_create_stores && allows_trick_play( options ) )
				{
					if ( position( ) < 0 )
						set_position( 0 );
					else if ( position( ) >= input->get_frames( ) )
						set_position( input->get_frames( ) - 1 );
				}

				const int pos = position( );

				if ( ( incomplete && frames_since_sync ++ > 100 ) || pos == input->get_frames( ) - 1 )
				{
					input->sync( );
					incomplete = !input->is_complete( );
					frames_since_sync = 0;
				}

				set_frames( input->get_frames( ) );

				status = input->fetch( frame, pos );
				if ( status == ml::fetch_eof ) break;
				if ( status == ml::fetch_error || frame == 0 || frame->in_error( ) ) RPANY_THROW( rpany::stack_exception, ( boost::format( "Error on frame %d" ) % pos ).str( ) );

				auto report = pl::pcos::value< std::string >( frame->properties( ), pl::pcos::key::from_string( "frame_stats" ), "" );
				if ( stats != stats_off && report != "" )
				{
					auto width = static_cast< size_t >( get_max_width( stack ) );
					if ( report.size( ) > width ) report = report.substr( 0, width );
					std::cerr << '\r' << report << ( stats == stats_on ? '\r' : '\n' );
				}

				if ( stores.size( ) )
				{
					running = stores_push( frame );
					if ( running ) running = stores_event( input );
				}

				set_position( position( ) + speed( ) );

				if ( pitch_filter )
					pitch_filter->properties( ).get_property_with_string( "speed" ) = pitch( );
			}

			if ( stats == stats_on ) std::cerr << "\n";

			stores_complete( );

			if ( !allows_trick_play( options ) )
			{
				input = ml::input_type_ptr( );
				pitch_filter = ml::input_type_ptr( );
			}

			handle_interrupt_state( type );
		}

		inline bool is_interrupted( const std::string &type ) const
		{
			return type == "playout" && rpany::is_interrupted( );
		}

		inline void handle_interrupt_state( const std::string &type ) const
		{
			if ( type == "playout" )
				if ( rpany::is_interrupted( ) )
					RPANY_THROW( rpany::interrupted_exception, "Interrupted." );
		}

		void stop( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			stopped = true;
		}

		bool is_stopped( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return stopped;
		}

		bool is_running( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return running;
		}

		void set_speed( int speed )
		{
			std::unique_lock< std::mutex > lock( mutex );
			speed_ = speed;
		}

		int speed( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return speed_;
		}

		void set_pitch( double pitch )
		{
			std::unique_lock< std::mutex > lock( mutex );
			pitch_ = pitch;
		}

		double pitch( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return pitch_;
		}

		void set_position( int position )
		{
			std::unique_lock< std::mutex > lock( mutex );
			position_ = position;
		}

		int position( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return position_;
		}

		void set_frames( int frames )
		{
			std::unique_lock< std::mutex > lock( mutex );
			frames_ = frames;
		}

		int frames( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return frames_;
		}

		const void describe( const std::string &type ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			std::ostream &out = stack.output( );

			// Provide the input description
			out << "Input:" << std::endl << std::endl;;
			if ( graph_ != "" )
				out << graph_;
			else if ( stack.depth( ) )
				stack << "describe";
			out << std::endl;

			// Provide the output description
			out << "Output:" << std::endl << std::endl;
			vector_ptr list;
			stack << ( type + "-spec" ) << "list-to-vector" >> list;
			for ( auto i : *list )
				out << rpany::cast< std::string >( i ) << " ";
			out << std::endl;
		}

		void grab_audio( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			for( auto iter = stores.begin( ); iter != stores.end( ); iter ++ )
			{
				auto prop = ( *iter )->properties( ).get_property_with_string( "grab_audio" );
				if ( prop.valid( ) )
				{
					prop.set( 1 );
					prop.update( );
					break;
				}
			}
		}

		void set_fullscreen( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			for( auto iter = stores.begin( ); iter != stores.end( ); iter ++ )
				fullscreen = vmlbatch::toggle_fullscreen( *iter, fullscreen );
		}

	private:
		inline bool is_property( const std::string &token ) const
		{
			return token.find( '=' ) != std::string::npos && token.find( ':' ) > token.find( '=' );
		}

		inline bool is_smart_pusher_token( const std::string &token ) const
		{
			return token.find( "filter:" ) == 0 || token.find( "store:" ) == 0 || is_property( token );
		}

		inline ml::store_type_ptr create_smart_pusher_store( )
		{
			ml::input_type_ptr graph;
			stack >> graph;
			ml::store_type_ptr store = ml::create_store( "smart_puller:", ml::frame_type_ptr( ) );
			ARENFORCE_MSG( !!store, "Unable to create smart_puller:" );
			store->properties( ).get_property_with_string( "graph" ) = graph;
			return store;
		}

		vector_ptr stores_list( const std::string &type ) const
		{
			// Slight inconsistency with established naming convention here - the normal
			// "playout" type is provided by the "player-spec" word rather than "playout-spec"
			const std::string spec = type == "playout" ? "player-spec" : type + "-spec";

			// Convert the store spec for the type to a vector
			vector_ptr list;
			stack << spec << "list-to-vector" >> list;

			return list;
		}

		inline bool has_stores( const vector_ptr &list ) const
		{
			return list->size( ) > 0;
		}

		inline bool has_default_preview( const vector_ptr &list ) const
		{
			return list->size( ) == 1 && rpany::cast< std::string >( list->front( ) ) == "default-preview:";
		}

		inline bool has_preview( const vector_ptr &list ) const
		{
			return list->size( ) > 0 && rpany::cast< std::string >( list->front( ) ) == "preview:";
		}

		void stores_create( const std::string &type, const ml::frame_type_ptr &frame )
		{
			// Obtain the playout spec for the specified type
			vector_ptr list = stores_list( type );

			// We need to hold state for the inclusion of stack operations
			bool is_smart_pusher = false;
			int smart_closure = 0;

			// Iterate through the list provided and create the implied stores
			for ( auto i = list->begin( ); i != list->end( ); i ++ )
			{
				// Cast the rp::any object to a string here
				std::string token = rpany::cast< std::string >( *i );

				// Handle smart_pusher store definition
				if ( is_smart_pusher )
				{
					if ( token == "{" )
					{
						if ( smart_closure ++ == 0 )
							continue;
					}
					else if ( token == "}" )
					{
						if ( -- smart_closure == 0 )
							continue;
					}
					if ( smart_closure != 0 || is_smart_pusher_token( token ) )
					{
						stack << token;
						continue;
					}
					else
					{
						stores.push_back( create_smart_pusher_store( ) );
						is_smart_pusher = false;
					}
				}

				// Enter smart_pusher state if requested
				if ( token == "smart_pusher:" )
				{
					stack << token;
					is_smart_pusher = true;
					continue;
				}

				// In all other cases, assume standard stores and property assignments
				if ( token == "default-preview:" )
				{
					ml::store_type_ptr store = ml::create_store( "preview:", frame->shallow( ) );
					if ( !store ) RPANY_THROW( rpany::stack_exception, "Unable to create a store from '" + token + "'" );
					store->property( "video" ) = std::wstring( L"vmlbatch-video:" );
					store->property( "native" ) = 1.0;
					store->property( "audio_scrub" ) = 0;
					store->property( "timed" ) = 1;
					int audio_device = -1;
					stack << "audio-device" << "@" >> audio_device;
					if ( audio_device != -1 )
						store->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "@audio.device" ) ) = audio_device );
					stores.push_back( store );
				}
				else if ( !is_property( token ) )
				{
					ml::store_type_ptr store = ml::create_store( token != "sdl_video:" ? token : "vmlbatch-video:", frame->shallow( ) );
					if ( !store ) RPANY_THROW( rpany::stack_exception, "Unable to create a store from '" + token + "'" );
					if ( token == "preview:" ) store->property( "video" ) = std::wstring( L"vmlbatch-video:" );
					stores.push_back( store );
				}
				else
				{
					if ( stores.size( ) == 0 ) RPANY_THROW( rpany::stack_exception, "Property assignment to non-existent store '" + token + "'" );
					pl::pcos::property_container props = stores.back( )->properties( );
					dynamic_cast< vmlbatch::stack & >( stack ).assign( props, token );
				}
			}

			// Sanity check
			ARENFORCE_MSG( smart_closure == 0, "Unbalanced smart_pusher: closures detected" );

			// If the last store was a smart_pusher, then it will not have been submitted yet
			if ( is_smart_pusher )
				stores.push_back( create_smart_pusher_store( ) );

			// Initialise all stores
			for ( auto iter = stores.begin( ); iter != stores.end( ); iter ++ )
				if ( !( *iter )->init( ) )
					RPANY_THROW( rpany::stack_exception, "Unable to initialise store" );
		}

		ml::input_type_ptr find( const ml::input_type_ptr &graph, const std::string &filter )
		{
			ml::input_type_ptr result;
			if ( graph && graph->slot_count( ) )
			{
				if ( cls::to_string( graph->get_uri( ) ) != filter )
					for( size_t i = 0; !result && i < graph->slot_count( ); i ++ )
						result = find( graph->fetch_slot( i ), filter );
				else
					result = graph;
			}
			return result;
		}

		ml::input_type_ptr normalise( const std::string &type )
		{
			// Attempt to obtain an input from the graph
			ml::input_type_ptr result;
			const rp::any any = stack.pull( );
			if ( !rpany::is< ml::input_type_ptr >( any ) )
				RPANY_THROW( rpany::stack_exception, "Cannot obtain an input from '" + rpany::convert< std::string >( any ) + "'" );
			const auto &input = rpany::as< ml::input_type_ptr >( any );

			// Confirm that we can use the input before going further
			ml::frame_type_ptr frame;
			if ( input->fetch( frame ) != ml::fetch_ok )
				return ml::input_type_ptr( );

			// Obtain our store specification
			const vector_ptr list = stores_list( type );

			// Allow override of interactive use - if the value is -1, we treat this as 'auto'
			// detection, if 0 it's off, and if 1 it's on.
			const int interactive = obtain_stack_var( type, "interactive", -1 );

			// If we have no stores, then we can only iterate through the graph - no normalisation
			const bool have_stores = has_stores( list );

			// If the graph has a store, then we are not interactive unless explicitly stated
			const bool graph_has_store = find( input, "store" ) != 0;

			// We initialise the player specs with "default-preview:" - this is the only one that
			// normalise will apply to, and only when in isolation
			const bool have_default_preview = has_default_preview( list );

			// If we have a preview as the first store, by convention we can have some form of
			// interactive, unless interactive has been specifically turned off
			const bool have_preview = has_preview( list );

			// We'll determine if we're interactive or not now
			bool is_interactive = true;

			// Putting those rules together:
			if ( interactive == -1 )
				is_interactive = have_stores && ( have_preview || have_default_preview ) && !graph_has_store;
			else if ( interactive == 0 )
				is_interactive = false;

			// Inidcate that we should create stores if we have stores to use
			should_create_stores = is_interactive || ( have_stores && !graph_has_store ) || ( type == "playout" && have_stores && !have_default_preview );

			// If our input is seekable, and we have a preview, we allow all interaction
			const bool is_seekable = input->is_seekable( );

			// Determine options for interaction
			if ( is_interactive )
			{
				if ( is_seekable && !graph_has_store )
					options = allow_all;
				else if ( graph_has_store || input->get_frames( ) == 1 )
					options = allow_none;
				else
					options = allow_pause;
			}
			else
			{
				options = allow_none;
			}

			// We only normalise if we have the default preview
			if ( is_interactive && have_default_preview )
			{
				// Push the input to the stack
				stack << input;

				// Only add a threader if there isn't an upstream threader
				if ( !find( input, "threader" ) && !find( input, "distributor" ) )
					stack << "n-thread";

				// Apply the requested normalisation
				stack << "normalise";

				// Pull the result
				result = stack.pull< ml::input_type_ptr >( );
			}
			else
			{
				// No normalisation applied
				result = input;
			}

			// Look for a pitch_filter
			auto search = result;
			while( !pitch_filter && search != input )
			{
				if ( search->properties( ).get_property_with_string( "speed" ).valid( ) )
					pitch_filter = search;
				else
					search = search->fetch_slot( );
			}

			// We will override the normalisation if necessary to support interaction
			if ( options == allow_all && !pitch_filter )
			{
				stack << result;
				stack << "filter:pitch" << "speed=1.0";
				pitch_filter = stack.cast< ml::input_type_ptr >( );
				stack << "filter:smart_sleep";
				stack >> result;
			}

			// Force a sync now
			result->sync( );

			// If we have a threader, activate it now
			auto threader = find( result, "threader" );
			if ( threader )
				stack << threader << "active=1" << "drop";

			return result;
		}

		bool stores_event( const ml::input_type_ptr &input )
		{
			bool result = true;
			int total_frames = input->get_frames( );

			for( auto iter = stores.begin( ); iter != stores.end( ); iter ++ )
			{
				ml::store_type_ptr store = *iter;
				pl::pcos::property keydown = store->properties( ).get_property_with_string( "keydown" );
				if ( keydown.valid( ) && keydown.value< int >( ) != 0 )
				{
					result = handle_keydown( store, keydown.value< int >( ), total_frames );
					keydown.set( 0 );
				}
			}

			return result;
		}

		bool handle_keydown( ml::store_type_ptr &store, int key, int total_frames )
		{
			bool result = true;
			const auto pos = position( );

			if ( key == ml::keysym::esc )
				result = false;
			else if ( tolower( key ) == 'f' )
				fullscreen = toggle_fullscreen( store, fullscreen );
			else if ( tolower( key ) == '=' )
				set_grab_audio( store );
			else if ( allows_trick_play( options ) && tolower( key ) == 'j' )
				tap( -1, 0.1 );
			else if ( allows_pause( options ) && ( tolower( key ) == 'k' || key == ' ' ) )
				toggle( );
			else if ( allows_trick_play( options ) && tolower( key ) == 'l' )
				tap( 1, 0.1 );
			else if ( allows_trick_play( options ) && tolower( key ) == 'm' )
				tap( -1, -0.1 );
			else if ( allows_trick_play( options ) && tolower( key ) == ',' )
				tap( 1, -0.1 );
			else if ( allows_trick_play( options ) && tolower( key ) == 'a' )
				set_position( find_prev( pos ) );
			else if ( allows_trick_play( options ) && tolower( key ) == 's' )
				set_position( find_start( pos ) );
			else if ( allows_trick_play( options ) && tolower( key ) == 'd' )
				set_position( find_next( pos ) );
			else if ( allows_trick_play( options ) && key == '0' )
				set_position( 0 );
			else if ( allows_trick_play( options ) && tolower( key ) == 'g' )
				set_position( pos < 1000 ? 0 : pos - 1000 );
			else if ( allows_trick_play( options ) && tolower( key ) == 'h' )
				set_position( pos + 1000 >= total_frames ? total_frames - 1 : pos + 1000 );
			else if ( allows_trick_play( options ) && key == ml::keysym::up )
				set_position( pos < 100 ? 0 : pos - 100 );
			else if ( allows_trick_play( options ) && key == ml::keysym::down )
				set_position( pos + 100 >= total_frames ? total_frames - 1 : pos + 100 );
			else if ( allows_trick_play( options ) && key == ml::keysym::right )
				step_forward( total_frames );
			else if ( allows_trick_play( options ) && key == ml::keysym::left )
				step_backward( );
			else if ( allows_trick_play( options ) && ( key == ml::keysym::end || key == 'e' ) )
				set_position( total_frames - 1 );

			return result;
		}

		void toggle( )
		{
			set_speed( speed( ) != 0 ? 0 : 1 );
			set_pitch( 1.0 );
		}

		template < typename T >
		const T between( const T &value, const T &lower, const T &upper )
		{
			return std::max< const T >( std::min< const T >( value, upper ), lower );
		}

		void tap( int direction, double shift )
		{
			if ( speed( ) != direction )
				set_speed( direction );
			else if ( allows_pitch( options ) )
				set_pitch( between< double >( pitch( ) + shift, 0.2, 10.0 ) );
		}

		void step_forward( int total_frames )
		{
			if ( speed( ) != 0 )
				set_speed( 0 );
			else if ( position( ) < total_frames )
				set_position( position( ) + 1 );
		}

		void step_backward( )
		{
			if ( speed( ) != 0 )
				set_speed( 0 );
			else if ( position( ) > 0 )
				set_position( position( ) - 1 );
		}

		void serialise( const ml::input_type_ptr &input )
		{
			std::string result;

			stack
			<< input
			<< "filter:aml" << "filename=@"
			<< "$" << "stdout" << "prop-query"
			>> result
			<< "drop"
			;

			std::unique_lock< std::mutex > lock( mutex );
			graph_ = result;
		}

		bool stores_push( const ml::frame_type_ptr &frame )
		{
			bool result = true;
			for( auto iter = stores.begin( ); result && iter != stores.end( ); iter ++ )
				result = ( *iter )->push( frame->shallow( ) );
			return result;
		}

		void stores_complete( )
		{
			for( auto iter = stores.begin( ); iter != stores.end( ); iter ++ )
				( *iter )->complete( );
			stores = { };
		}

		// Provides a means to scrape the input, looking for components which behave
		// like playlists, and creates an index which allows us to navigate the list
		void analyse_playlist( const ml::input_type_ptr &input )
		{
			playlist_.clear( );
			ml::input_type_ptr playlist = find_playlist( input );
			if ( playlist != 0 )
			{
				int position = 0;
				for( size_t i = 0; i < playlist->slot_count( ); i ++ )
				{
					const ml::input_type_ptr &slot = playlist->fetch_slot( i );
					if ( slot->get_frames( ) == INT_MAX ) continue;
					if ( auto offset = std::dynamic_pointer_cast< ml::filter_offset_type >( slot ) )
					{
						playlist_.insert( offset->get_offset( ) );
					}
					else
					{
						playlist_.insert( position );
						position += slot->get_frames( );
					}
				}
			}
		}

		// Responsible for finding the acceptable playlist filters
		const ml::input_type_ptr find_playlist( const ml::input_type_ptr &input )
		{
			if ( std::dynamic_pointer_cast< ml::filter_playlist_type >( input ) || std::dynamic_pointer_cast< ml::filter_compositor_type >( input ) )
				return input;
			else if ( input != 0 && input->slot_count( ) > 0 )
				return find_playlist( input->fetch_slot( ) );
			return ml::input_type_ptr( );
		}

	public:
		// Returns the position of the next item in the playlist
		const int find_next( int position ) const
		{
			if ( playlist_.size( ) == 0 ) return position;
			auto iter = playlist_.lower_bound( position );
			return iter != playlist_.end( ) ? *iter : 0;
		}

		// Returns the position of the start of the current item in the playlist
		const int find_start( int position ) const
		{
			if ( playlist_.size( ) == 0 ) return 0;
			auto iter = playlist_.lower_bound( position );
			return iter != playlist_.begin( ) ? *( -- iter ) : 0;
		}

		// Returns the position of the start of the previous item in the playlist
		const int find_prev( int position ) const
		{
			if ( playlist_.size( ) == 0 ) return 0;
			auto iter = playlist_.lower_bound( find_start( position - 1 ) );
			return iter != playlist_.begin( ) ? *( -- iter ) : 0;
		}

	private:
		ml::input_type_ptr input;
		ml::input_type_ptr pitch_filter;

		std::vector< ml::store_type_ptr > stores;

		mutable std::mutex mutex;
		bool stopped = false;
		int position_ = 0;
		int speed_ = 1;
		double pitch_ = 1.0;
		int frames_ = 0;
		std::string graph_;

		rpany::stack_ptr stack_ptr;
		rpany::stack &stack;

		bool running = true;
		bool fullscreen = false;
		int store_position = 0;
		int options = allow_all;
		bool should_create_stores = true;

		std::set< int > playlist_;
};

inline rpany::stack_ptr player_stack( const std::string &type, rpany::stack &s )
{
	// Create the default rpany stack here (will be a vmlstack anyway)
	rpany::stack_ptr result = rpany::factory::create( );

	// Make sure this instance is authorised to do what it requires
	dynamic_cast< vmlbatch::stack & >( *result ).set_accepts( vmlbatch::accept_all );

	// Inherit the dictionary from the incoming stack
	result->inherit( s );

	// Pull the first item from the input stack and push to top of stack
	*result += s.pull( );

	return result;
}

extern std::atomic< int > id_counter;

struct player_thread
{
	std::thread thread;
	const std::string type_;
	player_t player;
	int id_;

	player_thread( const std::string &type, rpany::stack &s )
	: type_( type )
	, player( player_stack( type, s ) )
	, id_( id_counter ++ )
	{
	}

	~player_thread( )
	{
		if ( thread.joinable( ) )
		{
			player.stop( );
			thread.join( );
		}
	}

	void play( stats_type stats = stats_off )
	{
		if ( thread.joinable( ) )
			RPANY_THROW( rpany::stack_exception, "Player is not available for use" );
		player.play( type_, stats );
	}

	void start( )
	{
		if ( !thread.joinable( ) )
			thread = std::thread( [ this ]( ) { try { player.play( type_ ); } catch ( ... ) { } } );
	}

	const int id( ) const
	{
		return id_;
	}

	const std::string status( ) const
	{
		if ( !thread.joinable( ) )
			return "inactive";
		else if ( player.is_running( ) )
			return "running";
		else
			return "stopped";
	}

	void set_speed( int speed ) { player.set_speed( speed ); }
	void set_pitch( double pitch ) { player.set_pitch( pitch ); }
	void set_position( int position ) { player.set_position( position ); }
	int speed( ) { return player.speed( ); }
	double pitch( ) { return player.pitch( ); }
	int position( ) { return player.position( ); }
	int frames( ) { return player.frames( ); }
	const void describe( ) { return player.describe( type_ ); }
	void grab_audio( ) { player.grab_audio( ); }
	void toggle_fullscreen( ) { player.set_fullscreen( ); }
	void next( ) { player.set_position( player.find_next( player.position( ) ) ); }
	void prev( ) { player.set_position( player.find_prev( player.position( ) ) ); }
};

typedef std::shared_ptr< player_thread > player_ptr;

inline void dot_frame( rpany::stack &s )
{
	auto frame = s.pull< vmlbatch_frame_ptr >( );
	frame->set_position( 0 );

	auto pusher = ml::create_input( "nudger:" );
	pusher->push( std::move( frame->shallow( ) ) );
	s += pusher;
	s << "length=1" << "player";
}

inline void dot( rpany::stack &s )
{
	rp::any tos = s.pick( );
	if ( rpany::is< ml::input_type_ptr >( tos ) )
		s << "$" << "player-adjust" << "eval-word?" << "player";
	else if ( rpany::is< vmlbatch_frame_ptr >( tos ) )
		s << dot_frame;
	else
		s.output( ) << s.pull< std::string >( ) << std::endl;
}

inline rp::any player_to_string( const rp::any &any )
{
	std::ostringstream st;
	auto player = rpany::as< player_ptr >( any );
	st << "<job-" << player->id( ) << "-" << player->status( ) << ">";
	return std::string( st.str( ) );
}

RPANY_OPEN( job, rpany::stack &s )
	s.teach( "job-create", [] ( rpany::stack &s ) { s += player_ptr( new player_thread( "jobs", s ) ); } );
	s.teach( "job-run", [] ( rpany::stack &s ) { s.pull< player_ptr >( )->play( stats_off ); } );
	s.teach( "job-start", [] ( rpany::stack &s ) { s.coerce< player_ptr >( )->start( ); } );
	s.teach( "job-speed", [] ( rpany::stack &s ) { auto v = s.pull< int >( ); s.coerce< player_ptr >( )->set_speed( v ); } );
	s.teach( "job-pitch", [] ( rpany::stack &s ) { auto v = s.pull< double >( ); s.coerce< player_ptr >( )->set_pitch( v ); } );
	s.teach( "job-seek", [] ( rpany::stack &s ) { auto v = s.pull< int >( ); s.coerce< player_ptr >( )->set_position( v ); } );
	s.teach( "job-speed?", [] ( rpany::stack &s ) { s += s.coerce< player_ptr >( )->speed( ); } );
	s.teach( "job-pitch?", [] ( rpany::stack &s ) { s += s.coerce< player_ptr >( )->pitch( ); } );
	s.teach( "job-position?", [] ( rpany::stack &s ) { s += s.coerce< player_ptr >( )->position( ); } );
	s.teach( "job-frames?", [] ( rpany::stack &s ) { s += s.coerce< player_ptr >( )->frames( ); } );
	s.teach( "job-describe", [] ( rpany::stack &s ) { s.coerce< player_ptr >( )->describe( ); } );
	s.teach( "job-grab-audio", [] ( rpany::stack &s ) { s.coerce< player_ptr >( )->grab_audio( ); } );
	s.teach( "job-toggle-fullscreen", [] ( rpany::stack &s ) { s.coerce< player_ptr >( )->toggle_fullscreen( ); } );
	s.teach( "job-next", [] ( rpany::stack &s ) { s.coerce< player_ptr >( )->next( ); } );
	s.teach( "job-prev", [] ( rpany::stack &s ) { s.coerce< player_ptr >( )->prev( ); } );
	s.evaluate( ": jobs-spec [[ default-preview: ]] ;" );
	rpany::coercion( COERCE_KEY( player_ptr, std::string ), player_to_string );
RPANY_CLOSE( job );

RPANY_OPEN( player, rpany::stack &s )
	s.teach( "player", [] ( rpany::stack &s ) { player_thread( "playout", s ).play( static_cast< stats_type >( obtain_var< int >( s, "playout-stats", 1 ) ) ); } );
	s.teach( "preview", [] ( rpany::stack &s ) { s << "dup"; player_thread( "playout", s ).play(static_cast< stats_type >( obtain_var< int >( s, "playout-stats", 1 ) ) ); } );
	s.teach( ".", dot );
	s.evaluate( ": player-spec [[ default-preview: ]] ;" );
	s.evaluate( ": player-speed variable! playout-speed ;" );
	s.evaluate( ": player-position variable! playout-position ;" );
	s.evaluate( ": player-pitch variable! playout-pitch ;" );
	s.evaluate( ": player-interactive variable! playout-interactive ;" );
	s.evaluate( ": player-stats variable! playout-stats ;" );
	s.evaluate( ": no-stats 0 player-stats ;" );
	s.evaluate( ": player-flags variable! playout-flags ;" );
	// Set up the audio device - default to 'system default' of -1, override from env or later assignment
	s.evaluate( "variable audio-device" );
	s.evaluate( "-1 audio-device !" );
	auto device = std::getenv( "VML_AUDIO_DEVICE" );
	if ( device != 0 ) s << device << "audio-device" << "!";
RPANY_CLOSE( player );

RPANY_OPEN( normalise, rpany::stack &s )
	// Default implementations of normalisation filters - feel free to reimplement prior to playing a graph.
	// Note that none are applied if a store is detected within the graph.
	s.evaluate( ": n-thread filter:distributor queue=12 ;" );
	s.evaluate( ": n-visualise filter:visualise type=1 colourspace=yuv420p width=1280 height=720 sar_num=1 sar_den=1 ;" );
	s.evaluate( ": n-resample filter:resampler channels=2 frequency=48000 ;" );
	s.evaluate( ": n-conform filter:conform ;" );
	s.evaluate( ": n-deinterlace filter:deinterlace ;" );
	s.evaluate( ": n-pitch filter:pitch speed=1.0 ;" );
	s.evaluate( ": normalise n-visualise n-resample n-conform n-deinterlace n-pitch filter:sleep ;" );
RPANY_CLOSE( normalise );
