#pragma once

struct ml_query_traits : public pl::default_query_traits
{
	std::wstring libname( ) const
	{ return std::wstring( L"openmedialib" ); }

	std::wstring type( ) const
	{ return std::wstring( L"" ); }
};

inline void query_type( rpany::stack &stack, const std::string &type )
{
	typedef pl::discovery< ml_query_traits > discovery;
	const ml_query_traits query;
	discovery plugins( query );

	std::ostream &output = stack.output( );
	const size_t max_width = get_max_width( stack );

	typedef std::set< std::string > entries_type;
	typedef std::map< std::string, entries_type > available_type;

	available_type available;
	size_t width = 0;

	for ( auto i = plugins.begin( ); i != plugins.end( ); ++i )
	{
		const auto &files = i->filenames( );

		for ( auto f = files.begin( ); f != files.end( ); f ++ )
		{
			const std::string filename = cls::to_string( fs::path( *f ).filename( ).native( ) );
			const std::string label = cls::to_string( i->name( ) );
			const bool exists = fs::exists( cls::to_t_string( *f ) );
			const bool found = exists && type == cls::to_string( i->type( ) );
			width = exists && label.size( ) > width ? label.size( ) : width;
			if ( found )
			{
				const auto &contents = i->extension( );
				auto &entries = available[ cls::to_string( label ) ];
				for ( auto j = contents.begin( ); j != contents.end( ); ++j )
					entries.insert( cls::to_string( j->str( ) ) );
			}
		}
	}

	for ( auto i = available.begin( ); i != available.end( ); i ++ )
	{
		const std::string label = i->first + std::string( width - i->first.size( ), ' ' ) + ": ";
		size_t current_width = label.size( );
		size_t count = 0;

		output << label;

		for ( auto j = i->second.begin( ); j != i->second.end( ); ++j )
		{
			if ( count > 0 && current_width + j->size( ) >= max_width )
			{
				output << std::endl << label;
				current_width = label.size( );
				count = 0;
			}
			output << *j << " ";
			current_width += j->size( ) + 1;
			count ++;
		}

		output << std::endl;
	}
}

inline void query_report( rpany::stack &stack, const std::vector< std::string > &types )
{
	for ( auto i = types.begin( ); i != types.end( ); i ++ )
	{
		stack.output( ) << *i << ":" << std::endl << std::endl;
		query_type( stack, *i );
		stack.output( ) << std::endl;
	}
}

void list_available( rpany::stack &stack, const std::string &type )
{
	typedef pl::discovery< ml_query_traits > discovery;
	const ml_query_traits query;
	discovery plugins( query );

	int count = 0;

	for ( auto i = plugins.begin( ); i != plugins.end( ); ++i )
	{
		const auto &files = i->filenames( );

		for ( auto f = files.begin( ); f != files.end( ); f ++ )
		{
			const std::string filename = cls::to_string( fs::path( *f ).filename( ).native( ) );
			const std::string label = cl::str_util::to_string( filename );
			const bool exists = fs::exists( cls::to_t_string( *f ) );
			const bool found = exists && type == cls::to_string( i->type( ) );
			if ( found )
			{
				const auto &contents = i->extension( );
				for ( auto j = contents.begin( ); j != contents.end( ); ++j )
				{
					stack += cls::to_string( j->str( ) );
					count ++;
				}
			}
		}
	}

	stack += count;
}

RPANY_OPEN( available, rpany::stack &s )
	s.teach( "available", [] ( rpany::stack &s ) { query_report( s, { "input", "filter", "output" } ); } );
	s.teach( "see-inputs", [] ( rpany::stack &s ) { query_type( s, "input" ); } );
	s.teach( "see-filters", [] ( rpany::stack &s ) { query_type( s, "filter" ); } );
	s.teach( "see-stores", [] ( rpany::stack &s ) { query_type( s, "output" ); } );
	s.teach( "list-inputs", [] ( rpany::stack &s ) { list_available( s, "input" ); } );
	s.teach( "list-filters", [] ( rpany::stack &s ) { list_available( s, "filter" ); } );
	s.teach( "list-stores", [] ( rpany::stack &s ) { list_available( s, "output" ); } );
RPANY_CLOSE( available );

#if defined( WITH_RPANY_LINENOISE )
RPANY_OPEN( linenoise, rpany::stack &s )
	static bool done = false;
	if ( ! done && s.is_word( std::string( "linenoise-dict" ) ) )
	{
		s << "$" << "" << "list-inputs" << "linenoise-dict";
		s << "$" << "filter:" << "list-filters" << "linenoise-dict";
		s << "$" << "store:" << "list-stores" << "linenoise-dict";
		done = true;
	}
RPANY_CLOSE( linenoise );
#endif
