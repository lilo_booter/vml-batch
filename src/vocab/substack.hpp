#pragma once

RPANY_OPEN( vml_stack, rpany::stack &s )
	typedef std::shared_ptr< stack > ptr;
	rpany::substack::create< stack >( s );
	rpany::coercion( COERCE_KEY( ptr, std::string ), [] ( const rp::any & ) { return rp::any( std::string( "<vml-ss>" ) ); } );
RPANY_CLOSE( vml_stack );
