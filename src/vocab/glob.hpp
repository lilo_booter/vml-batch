#pragma once

#ifndef _MSC_VER

inline void glob( rpany::stack &s )
{
	std::string pat = s.pull< std::string >( );
	int count = 0;

	glob_t glob_result;
	::glob( pat.c_str( ), GLOB_TILDE, NULL, &glob_result );
	for( unsigned int i = 0; i < glob_result.gl_pathc; ++ i, ++ count )
		s += std::string( glob_result.gl_pathv[ i ] );
	globfree( &glob_result );

	s.append( count );
}

RPANY_OPEN( glob, rpany::stack &s )
	s.teach( "glob", glob );
	s.teach( "glob-list", { ">r-list", 0, "r>", 0, "do", "r>", "glob", "[+]", "loop" } );
RPANY_CLOSE( glob );

#endif
