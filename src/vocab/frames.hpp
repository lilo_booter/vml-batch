#pragma once

inline void fetch( rpany::stack &stack )
{
	auto i = stack.pull< int >( );
	if ( i < 0 ) i += stack.cast< ml::input_type_ptr >( )->get_frames( );
	ml::frame_type_ptr f;
	ml::fetch_status s = stack.cast< ml::input_type_ptr >( )->fetch( f, i );
	if ( s == ml::fetch_eof ) RPANY_THROW_DEFAULT( rpany::leave_exception );
	else if ( s == ml::fetch_error ) RPANY_THROW( rpany::stack_exception, "Unable to obtain frame" );
	else stack += vmlbatch_frame_ptr( f.release( ) );
}

inline void frame_is( rpany::stack &stack )
{
	stack += rpany::is< vmlbatch_frame_ptr >( stack.pick( ) );
}

inline void has_image( rpany::stack &stack )
{
	stack += stack.cast< vmlbatch_frame_ptr >( )->has_image( );
}

inline void has_alpha( rpany::stack &stack )
{
	stack += stack.cast< vmlbatch_frame_ptr >( )->has_alpha( );
}

inline void has_audio( rpany::stack &stack )
{
	stack += stack.cast< vmlbatch_frame_ptr >( )->has_audio( );
}

inline void frame_fps( rpany::stack &stack )
{
	const auto &frame = stack.cast< vmlbatch_frame_ptr >( );
	stack += rpany::rational( frame->get_fps_num( ), frame->get_fps_den( ) );
}

inline void frame_dim( rpany::stack &stack )
{
	const auto &frame = stack.cast< vmlbatch_frame_ptr >( );
	if ( !frame->has_image( ) ) RPANY_THROW( rpany::stack_exception, "Frame lacks an image" );
	stack += frame->width( );
	stack += frame->height( );
}

inline void frame_sar( rpany::stack &stack )
{
	const auto &frame = stack.cast< vmlbatch_frame_ptr >( );
	if ( !frame->has_image( ) ) RPANY_THROW( rpany::stack_exception, "Frame lacks an image" );
	stack += rpany::rational( frame->get_sar_num( ), frame->get_sar_den( ) );
}

inline void frame_audio( rpany::stack &stack )
{
	const auto &frame = stack.cast< vmlbatch_frame_ptr >( );
	if ( !frame->has_audio( ) ) RPANY_THROW( rpany::stack_exception, "Frame lacks audio" );
	stack += frame->frequency( );
	stack += frame->channels( );
}

inline rp::any frame_to_string( const rp::any &any )
{
	return std::string( "<frame>" );
}

RPANY_OPEN( frames, rpany::stack &s )
	s.teach( "fetch", fetch );
	s.teach( "frame?", frame_is );
	s.teach( "has-image?", has_image );
	s.teach( "has-alpha?", has_alpha );
	s.teach( "has-audio?", has_audio );
	s.teach( "frame-fps", frame_fps );
	s.teach( "frame-dim", frame_dim );
	s.teach( "frame-sar", frame_sar );
	s.teach( "frame-audio", frame_audio );
	rpany::coercion( COERCE_KEY( vmlbatch_frame_ptr, std::string ), frame_to_string );
RPANY_CLOSE( frames );

class timecode
{
	public:
		timecode( int position, const rpany::rational &fps, bool df )
		: position_( position )
		, fps_( fps )
		, df_( df )
		{
			timecode_ = cl::str_util::to_string( cl::frames_to_time_code( position_, cl_fps( ), df_, true ).to_string( ) );
		}

		timecode( const std::string &timecode, const rpany::rational &fps )
		: timecode_( timecode )
		, fps_( fps )
		, df_( timecode.find( ';' ) != std::string::npos || timecode.find( '.' ) != std::string::npos )
		{
			cl::time_code tc( cl::str_util::to_t_string( timecode_ ) );
			position_ = cl::time_code_to_frames( cl_fps( ), tc );
		}

		const int position( ) const { return position_; }
		const std::string &to_string( ) const { return timecode_; }
		const rpany::rational &fps( ) const { return fps_; }
		const bool df( ) const { return df_; }

		const timecode add( timecode &other )
		{
			if ( fps( ) != other.fps( ) || df_ != other.df( ) )
				RPANY_THROW( rpany::stack_exception, "ERROR: Cannot add timecodes with different framerates or dropframes" );
			return timecode( position( ) + other.position( ), fps_, df_ );
		}

		const timecode subtract( timecode &other )
		{
			if ( fps( ) != other.fps( ) || df_ != other.df( ) )
				RPANY_THROW( rpany::stack_exception, "ERROR: Cannot subtract two timecodes with different framerates or dropframes" );
			return timecode( position( ) - other.position( ), fps_, df_ );
		}

	private:
		const cl::rational_time cl_fps( ) const { return cl::rational_time( fps_.numerator( ), fps_.denominator( ) ); }

		int position_;
		std::string timecode_;
		const rpany::rational fps_;
		const bool df_;
};

inline void timecode_fps( rpany::stack &stack )
{
	if ( stack.vfind( "tc-fps-var" ) == stack.vend( ) )
		RPANY_THROW( rpany::stack_exception, "FPS not specified - use <fps> tc-set-fps" );
	stack << "tc-fps-var" << "@";
}

inline void timecode_set_fps( rpany::stack &stack )
{
	stack << "variable!" << "tc-fps-var";
}

inline rpany::rational timecode_get_fps( rpany::stack &stack )
{
	timecode_fps( stack );
	return stack.pull< rpany::rational >( );
}

inline void to_timecode( rpany::stack &stack )
{
	rp::any tos = stack.pick( );
	if ( rpany::is_numeric( tos ) )
		stack += timecode( stack.pull< int >( ), timecode_get_fps( stack ), false );
	else if ( !rpany::is< timecode >( tos ) )
		stack += timecode( stack.pull< std::string >( ), timecode_get_fps( stack ) );
}

inline void to_timecode_df( rpany::stack &stack )
{
	stack += timecode( stack.pull< int >( ), timecode_get_fps( stack ), true );
}

inline void timecode_to_position( rpany::stack &stack )
{
	const auto tc = stack.pull< timecode >( );
	stack += tc.position( );
}

inline timecode timecode_pull( rpany::stack &stack )
{
	stack << to_timecode;
	return stack.pull< timecode >( );
}

inline void timecode_add( rpany::stack &stack )
{
	auto tc1 = timecode_pull( stack );
	auto tc0 = timecode_pull( stack );
	stack += tc0.add( tc1 );
}

inline void timecode_subtract( rpany::stack &stack )
{
	auto tc1 = timecode_pull( stack );
	auto tc0 = timecode_pull( stack );
	stack += tc0.subtract( tc1 );
}

RPANY_OPEN( timecodes, rpany::stack &s )
	s.evaluate( ": tc inline parse [[ to-tc ]] ;" );
	s.evaluate( ": tc-df inline parse [[ to-tc-df ]] ;" );
	s.teach( "to-tc", to_timecode );
	s.teach( "to-tc-df", to_timecode_df );
	s.teach( "tc-fps", timecode_fps );
	s.teach( "tc-set-fps", timecode_set_fps );
	s.teach( "tc-to-position", timecode_to_position );
	s.teach( "tc-add", timecode_add );
	s.teach( "tc-subtract", timecode_subtract );
	rpany::coercion( COERCE_KEY( timecode, std::string ), [ ] ( const rp::any &any ) { return rp::any( rpany::as< timecode >( any ).to_string( ) ); } );
	rpany::coercion( COERCE_KEY( timecode, int ), [ ] ( const rp::any &any ) { return rp::any( rpany::as< timecode >( any ).position( ) ); } );
	rpany::coercion( COERCE_KEY( timecode, double ), [ ] ( const rp::any &any ) { return rp::any( static_cast< double >( rpany::as< timecode >( any ).position( ) ) ); } );
RPANY_CLOSE( timecodes );
