#pragma once

#undef read
#undef write

inline const std::string layer_type( const ml::io::access_mode::enum_type &mode )
{
	switch( mode )
	{
		case ml::io::access_mode::read: return "read";
		case ml::io::access_mode::write: return "write";
		#ifdef VML_LAYER_HAS_READ_WRITE
		case ml::io::access_mode::read_write: return "read+";
		#endif
	}
	return "";
}

struct layer_wrapper
{
	typedef std::shared_ptr< layer_wrapper > ptr;

	layer_wrapper( const std::string &uri_, ml::io::access_mode::enum_type mode_ )
	: uri( uri_ )
	, mode( mode_ )
	, io( ml::io::get_layer( uri, mode, prefix ) )
	{
		if ( !io ) RPANY_THROW( rpany::stack_exception, "Unable to create layer: " + uri );
		if ( prefix != "" ) RPANY_THROW( rpany::stack_exception, "Unrecognised prefix on layer: " + prefix );
		if ( io->open( ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to open layer for " + type( ) );
	}

	~layer_wrapper( )
	{
		if ( io )
		{
			if ( mode == ml::io::access_mode::write )
			{
				if ( io->flush( ) != 0 )
				{
					ARLOG_ERR( "Unable to flush" );
				}
			}

			if ( io->close( ) != 0 )
			{
				ARLOG_ERR( "Unable to close" );
			}
		}
	}

	const std::string type( )
	{
		return layer_type( mode );
	}

	const std::string uri;
	ml::io::access_mode::enum_type mode;
	std::string prefix;
	ml::io::layer_ptr io;
	uint8_t buffer[ 1472 ];
	uint32_t used = 0;
};

inline rp::any layer_to_string( const rp::any &any )
{
	const auto input = rpany::as< layer_wrapper::ptr >( any );
	return input ? std::string( "<io:" ) + input->type( ) + ":" + input->uri + ">" : "<io>";
}

inline void layer_open_read( rpany::stack &s )
{
	std::string uri = s.pull< std::string >( );
	s.append( layer_wrapper::ptr( new layer_wrapper( uri, ml::io::access_mode::read ) ) );
}

#ifdef VML_LAYER_HAS_READ_WRITE
inline void layer_open_read_write( rpany::stack &s )
{
	std::string uri = s.pull< std::string >( );
	s.append( layer_wrapper::ptr( new layer_wrapper( uri, ml::io::access_mode::read_write ) ) );
}
#endif

inline void layer_open_write( rpany::stack &s )
{
	std::string uri = s.pull< std::string >( );
	s.append( layer_wrapper::ptr( new layer_wrapper( uri, ml::io::access_mode::write ) ) );
}

inline void noop( ) { }

inline void layer_readline( rpany::stack &s )
{
	auto src = s.cast< layer_wrapper::ptr >( );
	std::string result;
	unsigned char c;
	uint32_t n;
	// The layer api insists that we check on return values, but this is nuts really - it
	// errors when there's nothing to flush or if it's an invald option for the layer, but
	// there is no 'can_flush/should_flush' type method - so all you can do is ignore the 
	// bloody thing.
	if ( src->io->flush( ) != 0 ) noop( );
	while( ( 0 == src->io->read( &c, 1, n ) ) && n > 0 )
	{
		result += c;
		if ( c < ' ' ) break;
	}
	s += result;
}

inline void layer_size( rpany::stack &s )
{
	auto layer = s.cast< layer_wrapper::ptr >( );
	int64_t size = 0;
	if ( layer->io->size( size ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to size layer" );
	s.append( uint64_t( size ) );
}

inline void layer_remains( rpany::stack &s )
{
	auto layer = s.cast< layer_wrapper::ptr >( );
	int64_t size = 0;
	if ( layer->io->size( size ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to size layer" );
	s.append( uint64_t( size - layer->io->tell( ) ) );
}

inline void layer_tell( rpany::stack &s )
{
	auto layer = s.cast< layer_wrapper::ptr >( );
	s.append( uint64_t( layer->io->tell( ) ) );
}

inline void layer_seek( rpany::stack &s )
{
	auto position = s.pull< uint64_t >( );
	auto layer = s.cast< layer_wrapper::ptr >( );
	if ( layer->io->seek( int64_t( position ), ml::io::beginning ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to seek" );
}

inline void layer_sync( rpany::stack &s )
{
	auto layer = s.cast< layer_wrapper::ptr >( );
	int64_t size = 0;
	if ( layer->io->sync( size ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to sync layer" );
	s.append( uint64_t( size ) );
}

inline void layer_transfer( rpany::stack &s )
{
	uint64_t send = s.pull< uint64_t >( );
	auto src = s.cast< layer_wrapper::ptr >( );
	auto dst = s.cast< layer_wrapper::ptr >( 1 );
	uint64_t sent = 0;
	// See rant above
	if ( src->io->flush( ) != 0 ) noop( );
	while ( send > 0 )
	{
		auto bytes = send > sizeof( src->buffer ) ? sizeof( src->buffer ) : send;
		auto error = src->io->read( src->buffer, static_cast< uint32_t >( bytes ), src->used );
		if ( src->used == 0 ) break;
		uint32_t out = 0;
		if ( dst->io->write( src->buffer, src->used, out ) != 0 ) RPANY_THROW( rpany::stack_exception, "Failed to write to " + dst->uri );
		send -= src->used;
		sent += out;
		if ( error != 0 ) break;
	}
	if ( dst->io->flush( ) != 0 ) RPANY_THROW( rpany::stack_exception, "Failed to flush " + dst->uri );
	s.append( sent );
}

inline void layer_to_stdout( rpany::stack &s )
{
	auto src = s.cast< layer_wrapper::ptr >( );
	while ( true )
	{
		auto bytes = sizeof( src->buffer );
		auto error = src->io->read( src->buffer, static_cast< uint32_t >( bytes ), src->used );
		if ( src->used == 0 ) break;
		if ( fwrite( src->buffer, src->used, 1, stdout ) != 1 ) RPANY_THROW( rpany::stack_exception, "Failed to write to stdout" );
		if ( fflush( stdout ) != 0 ) RPANY_THROW( rpany::stack_exception, "Failed to flush stdout" );
		if ( error != 0 ) break;
	}
}

inline void layer_from_stdin( rpany::stack &s )
{
	auto dst = s.cast< layer_wrapper::ptr >( );
	while ( true )
	{
		auto bytes = fread( dst->buffer, 1, sizeof( dst->buffer ), stdin );
		if ( bytes == 0 ) break;
		uint32_t out = 0;
		if ( dst->io->write( dst->buffer, bytes, out ) != 0 ) RPANY_THROW( rpany::stack_exception, "Failed to write to " + dst->uri );
		if ( out != bytes ) break;
		if ( dst->io->flush( ) != 0 ) RPANY_THROW( rpany::stack_exception, "Failed to flush to " + dst->uri );
	}
}

inline void layer_concat( rpany::stack &s )
{
	layer_remains( s );
	layer_transfer( s );
}

inline void layer_flush( rpany::stack &s )
{
	auto layer = s.cast< layer_wrapper::ptr >( );
	if ( layer->io->flush( ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to flush layer" );
}

inline void layer_write( rpany::stack &s )
{
	auto message = s.pull< std::string >( );
	auto layer = s.cast< layer_wrapper::ptr >( );
	if ( layer->io->write( ( const unsigned char * )message.c_str( ), static_cast< uint32_t >( message.size( ) ) ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to write" );
}

inline void layer_emit( rpany::stack &s )
{
	auto ch = static_cast< unsigned char >( s.pull< int >( ) );
	auto layer = s.cast< layer_wrapper::ptr >( );
	if ( layer->io->write( &ch, 1 ) != 0 ) RPANY_THROW( rpany::stack_exception, "Unable to write" );
}

inline void chomp( rpany::stack &s )
{
	auto message = s.pull< std::string >( );
	auto size = message.size( );
	while( size > 0 && ( message[ size - 1 ] == 10 || message[ size - 1 ] == 13 ) )
		size --;
	message.resize( size );
	s += message;
}

RPANY_OPEN( layers, rpany::stack &s )
	s.teach( "io-open-read", [] ( rpany::stack &s ) { layer_open_read( s ); } );
	#ifdef VML_LAYER_HAS_READ_WRITE
	s.teach( "io-open-read+", [] ( rpany::stack &s ) { layer_open_read_write( s ); } );
	#endif
	s.teach( "io-open-write", [] ( rpany::stack &s ) { layer_open_write( s ); } );
	s.teach( "io-size", [] ( rpany::stack &s ) { layer_size( s ); } );
	s.teach( "io-tell", [] ( rpany::stack &s ) { layer_tell( s ); } );
	s.teach( "io-remains", [] ( rpany::stack &s ) { layer_remains( s ); } );
	s.teach( "io-sync", [] ( rpany::stack &s ) { layer_sync( s ); } );
	s.teach( "io-readline", [] ( rpany::stack &s ) { layer_readline( s ); } );
	s.teach( "io-transfer", [] ( rpany::stack &s ) { layer_transfer( s ); } );
	s.teach( "io-to-stdout", [] ( rpany::stack &s ) { layer_to_stdout( s ); } );
	s.teach( "io-from-stdin", [] ( rpany::stack &s ) { layer_from_stdin( s ); } );
	s.teach( "io-concat", [] ( rpany::stack &s ) { layer_concat( s ); } );
	s.teach( "io-flush", [] ( rpany::stack &s ) { layer_flush( s ); } );
	s.teach( "io-seek", [] ( rpany::stack &s ) { layer_seek( s ); } );
	s.teach( "io-write", [] ( rpany::stack &s ) { layer_write( s ); } );
	s.teach( "io-emit", [] ( rpany::stack &s ) { layer_emit( s ); } );
	s.teach( "chomp", chomp );
	rpany::coercion( COERCE_KEY( layer_wrapper::ptr, std::string ), layer_to_string );
RPANY_CLOSE( layers );
