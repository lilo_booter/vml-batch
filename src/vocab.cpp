#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openmedialib/ml/keysym.hpp>
#include <boost/array.hpp>
#include <atomic>

#include <rpany/rpany-host.hpp>

#if defined( WITH_RPANY_COMMS )
#include <rpany/rpany-comms.hpp>
#endif

#if defined( WITH_RPANY_LINENOISE )
#include <rpany/rpany-linenoise.hpp>
#endif

#include "vmlstack.hpp"

#ifndef _MSC_VER
#include <glob.h>
#endif

namespace ml = olib::openmedialib::ml;
namespace pl = olib::openpluginlib;
namespace cl = olib::opencorelib;
namespace cls = olib::opencorelib::str_util;
namespace fs = boost::filesystem;

namespace vmlbatch {

#include "vocab/init.hpp"
#include "vocab/system.hpp"
#include "vocab/graphs.hpp"
#include "vocab/frames.hpp"
#include "vocab/properties.hpp"
#include "vocab/substack.hpp"
#include "vocab/player.hpp"
#include "vocab/available.hpp"
#include "vocab/glob.hpp"
#include "vocab/vars.hpp"
#include "vocab/layers.hpp"

}
