// vmlbatch main function
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.

#include <rpany/rpany.hpp>

#include "init.hpp"
#include "vmlstack.hpp"

#include <vmlbatch_preview.hpp>

// Image caching is on by default
bool image_cache = true;

// Provides a factory method to create a stack instance
inline rpany::stack_ptr create( )
{
	return rpany::stack_ptr( new vmlbatch::stack );
}

// If we want to enable logging as soon as the process starts, we need to activate
// it by way of an env var as the command line is processed after the vml system is
// initialised.
int get_log_level( const std::string &var = "VMLBATCH_LOGLEVEL" )
{
	const char *value = getenv( var.c_str( ) );

	try
	{
		return value ? boost::lexical_cast< int >( value ) : -1;
	}
	catch ( ... )
	{
		std::cerr << "WARNING: Invalid log level of '" << value << "' specified - using max log level" << std::endl;
		return 99;
	}
}

// Provides a means to handle non-stack command line arguments - this function
// can provide additional switch handling if required.
bool arg_handler( rpany::stack &s, int argc, char **argv, int &index )
{
	// Convert the arg pointed at by the index to string
	const std::string arg = std::string( argv[ index ] );

	// Support amlbatch style [ -- <store>* ] handling
	if ( arg == "--" )
	{
		// Simply rewrite player-spec with remainder of args
		s << ":" << "player-spec" << "[[";
		while ( ++ index < argc && std::string( argv[ index ] ).find( "--" ) != 0 )
			s << argv[ index ];
		s << "]]" << ";";

		return true;
	}
	// Check if the current argument represents a valid switch
	else if ( arg == "--no-stats" )
		s << "no-stats";
	else if ( arg == "--player-flags" && index < argc - 1 )
		s << boost::lexical_cast< int >( argv[ ++ index ] ) << "player-flags";
	else if ( arg == "--audio-device" && index < argc - 1 )
		s << boost::lexical_cast< int >( argv[ ++ index ] ) << "audio-device" << "!";
	else if ( arg == "--interactive" )
		s << 1 << "player-interactive";
	else if ( arg == "--non-interactive" )
		s << 0 << "player-interactive";
	else if ( arg == "--no-normalise" )
		rpany::eval( s, ": normalise ;" );
	else if ( arg == "--no-image-cache" )
		ml::image::configure_global_cache( ml::image::image_pool_ptr( ) );
	else if ( arg == "--seek" && index < argc - 1 )
		s << 0 << "player-speed" << boost::lexical_cast< int >( argv[ ++ index ] ) << "player-position";
	else if ( arg == "--iterate" )
		rpany::eval( s, ": player-spec [[ ]] ;" );
	else
		return false;

	// If we get here, we assume that the current arg has been accepted.
	index ++;

	return true;
}

int usage( )
{
	std::cout << "Usage: vmlbatch <graph> [ -- ( <store> )* ]" << std::endl;
	return 0;
}

int vmlbatch_main( int argc, char **argv )
{
	// Default return to the optimsitic "no errors"
	int error = 0;

	// Handle arguments and evaluate results - defensive use of try/catch here
	try
	{
		// Initialise the vml system
		vmlbatch::init( get_log_level( ) );

		// Register our factory method
		rpany::factory::enroll( create );

		// Create a vmlstack
		vmlbatch::stack stack;

		// Register the default signal handler
		rpany::register_signal_handler( );

		// Register all vocabs
		rpany::educate( stack );

		// Determine if the terminal is a tty
		const bool tty = rpany::is_tty( );

		// If no arguments are given, run help and exit
		if ( tty && argc == 1 )
			return usage( );

		// Turn on image cache
		ml::image::configure_global_cache( ml::image::create_image_pool( ) );

		// Parse command line args and exit on failure
		error = rpany::eval( stack, argc, argv, 1, arg_handler );

		// We need to "dot" the result left at the top of stack
		if ( error == 0 && stack.depth( ) != 0 )
			error = rpany::eval( stack, "." );
	}
	catch( std::exception &e )
	{
		std::cerr << "ERROR: Failure while processing stack: " << e.what( ) << std::endl;
		error = 1;
	}
	catch( ... )
	{
		std::cerr << "FATAL: Unknown exception thrown while processing stack" << std::endl;
		error = 1;
	}

	return error;
}

struct vmlbatch_thread
{
	std::thread thread;
	vmlbatch_thread( int argc, char **argv ) { thread = std::thread( [ argc, argv ] ( ) { vmlbatch::schedule( vmlbatch::task_exit{ vmlbatch_main( argc, argv ) } ); } ); }
	~vmlbatch_thread( ) { if ( thread.joinable( ) ) thread.join( ); }
};

int main( int argc, char **argv )
{
	vmlbatch_thread thread( argc, argv );
	return vmlbatch::process( );
}
