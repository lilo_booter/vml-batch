# vml-batch scripts

Assorted vmlbatch, shell and python utilities are found here.

## vmlshell

Provides an interactive shell which can access the vmlbatch functionality. Can
be used for building graphs, playout, communications, transcoding, analysing,
debugging etc.

## vmltutor

vmltutor consists of 3 scripts:

* vmldocs - generates vml script by scraping source files for relevant comments
* vmltutor - once vmldocs output is correctly placed, vmltutor provides a
  commmand line tool to accesss the info for each component
* vml2web - render all output from vmltutor as a single html file

## vml-completions.sh

Provides bash completion for vmlbatch or derived scripts.

## mkurl

Misplaced script which is so lightweight and small, it doesn't warrant its own
repo. Provides a means to share local files by way of an installed http server,
the users www directory and sym links.

There is currently no support for this on windows.

## vml2www

Converts an aml graph dump which references local files to a form which can be
shared others by way of http - uses mkurl internally.

## vml2dot

Converts and aml graph dump to a graphviz dot file.

## vmlclean

Removes all default property assignments from the input filter graph, thus
making it easier to identify application and user provided settings.
