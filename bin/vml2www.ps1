if ( "$Env:VML_PYTHON3" -ne "" -and ( Test-Path "$Env:VML_PYTHON3" ) ) {
	Invoke-Expression "$Env:VML_PYTHON3" "$PSScriptRoot/vml2www" ${args}
} else {
	echo "Assign VML_PYTHON3 to a windows native python 3 executable"
}
