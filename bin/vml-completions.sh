#!/usr/bin/env bash

# Provides auto completion logic for vmlbatch based tool of your choice.
#
# Currently:
#
# * Completes filters using filter:[tab][tab]
#
# * Provides some assistance with inputs
#
# * Provides help information about a filter using: filter:name?[tab]
#
#   The help information is displayed and the ? is automatically removed.
#
# * Provides vml 'word' completion for any words that are defined in the tool.
#
# * Provides a means to list filter properties using filter:name ... %[tab][tab]
#
#   There is currently no mechanism to allow completion of these though.
#
# * All files are completed as per usual.

if [[ "$1" != "" ]]
then
	# The completion system in bash does not treat colons the way we use them here
	COMP_WORDBREAKS=${COMP_WORDBREAKS/:/}

	# Currently, only 1 tool is supported per bash instance
	vmltool=$1

	# Extract filters inputs and stores
	vmlfilters=( $( vmltutor --filter | grep "^--.*$" | sed -e 's/^--//' -e 's/-/:/' ) )
	vmlinputs=( $( vmltutor --input | grep "^--input-" | sed -e 's/^--input-//' ) )
	vmlstores=( $( vmltutor --store | grep "^--store-" | sed -e 's/^--store-//' ) )

	# Extract words
	vmlwords=( $( $vmltool words ) )

	# Dictionary used to retain filter properties
	declare -A vmlprops

	# _vmlprops filter:name
	#
	# Outputs the default properties associated to the named filter

	_vmlprops() {
		local name=${1/\\/}
		if [[ ! "${vmlprops[$name]+_}" && "$name" == "filter:"* ]] ; then
			vmlprops["$name"]=$( vmlbatch \$ "$name" prop-report )
		elif [[ ! "${vmlprops[$name]+_}" ]] ; then
			vmlprops["$name"]=$( vmlbatch \$ "input:$name" prop-report )
		fi
		printf "%s\n" "${vmlprops[$name]}"
	}

	# _vmlobject
	#
	# In the context of bash completion, determine the last specified filter or input.

	_vmlobject() {
		local sub=( ${COMP_LINE:0:$COMP_POINT} )
		local index=${#sub[@]}
		local token
		for (( index -- ; index >= 0 ; index -- )) ; do
			token=${sub[$index]}
			if [[ "$token" != *"="* && "$token" != "%" && "$token" != "?" ]] ; then
				echo "$token"
				break
			fi
		done
	}

	# _vmlrerender
	#
	# Re-renders the command line after an output producing tab completion is invoked

	_vmlrerender() {
		echo -n "${PS1@P}${COMP_LINE:0:$COMP_POINT}"
		IFS=$'\e[;' read -srd R -p $'\e[6n' _ _ l c
		echo -n "${COMP_LINE:$COMP_POINT}"
		let remainder=${#COMP_LINE}-COMP_POINT
		let lines_in_remainder=(c+remainder)/COLUMNS
		if (( l + lines_in_remainder > LINES )) ; then
			let extra_lines=(l+lines_in_remainder)-LINES
			let l=l-extra_lines
		fi
		echo -ne "\E[${l};${c}H"
	}

	# _vmlcomplete
	#
	# Invoked by bash completion

	_vmlcomplete() {
		local cur object copy

		_get_comp_words_by_ref -n : cur

		if [[ "$cur" == "%" ]] ; then
			object=$( _vmlobject )
			COMPREPLY=( $( _vmlprops "$object" ) )
		elif [[ "$cur" == *"?" ]] ; then
			cur=${cur/\?/}
			cur=${cur/\\/}
			# TODO: Migrate everything to vmlutor to allow it to select the filter, input, store or word
			echo
			if [[ "$cur" == "filter:"* ]] ; then
				vmltutor --${cur/:/-} | less
			elif [[ "$cur" == "store:"* ]] ; then
				vmltutor --${cur/:/-} | less
			else
				local copy=${cur//\"/}
				vmltutor "--input-$copy" | less
			fi
			_vmlrerender
			COMPREPLY=( "$cur" )
		elif [[ "$cur" != "" ]] ; then
			COMPREPLY=( $( printf '%s\n' "${vmlinputs[@]}" "${vmlfilters[@]}" "${vmlwords[@]}" | grep "^$cur" | grep -v "\*"  ) )
		elif [[ "$cur" == "" ]] ; then
			COMPREPLY=( $( printf '%s\n' "${vmlinputs[@]}" "${vmlwords[@]}" | grep -v "\*"  ) )
		fi

		__ltrim_colon_completions "$cur"
	}

	# Clear the previously specified completion rules for this tool (if any)
	complete -r "$vmltool" 2> /dev/null

	# Register the completion rules for the specified tools
	complete -F _vmlcomplete -A file "$vmltool"

else
	echo "Usage: source vml-completion.sh vmlbatch [or vmlbatch script]"
fi

