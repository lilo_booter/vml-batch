#!/usr/bin/env python3

import sys
import os
import platform
import requests.utils

# Get env var or use fallback if not specified
def getenv( var, fallback = "" ):
	result = os.getenv( var )
	if result is None: result = fallback
	return result

# Global state
base = getenv( "MKURL_BASE" )
domain = getenv( "MKURL_DOMAIN", ".local" )
host = getenv( "MKURL_HOST", platform.node( ) + domain )
prefix = getenv( "MKURL_PREFIX" )
suffix = getenv( "MKURL_SUFFIX" )
home = getenv( "MKURL_HOME", "%s/www" % os.getenv( "HOME" ) )

def help_switches( ):
	print( '''
    --url=<base-url>

        Entirely override the base-url generation - the full paths of the files are
        appended directly to this, so it does not need a trailing /.

        This can also be specified by a MKURL_BASE environment variable.

        When specified, none of the --host, --domain or --prefix switches or
        associated environment variables have any effect.

    --host=<host>

        Overrides the HOST used in the url - by default this is determined from the
        HOST environment variable.

        This can also be specified by a MKURL_HOST environment variable.

        When specified, neither the --domain switch or the associated environment
        variable have any effect.

    --domain=<domain>

        Overrides the DOMAIN used in the url - by default this is .local. If HOST is
        a full domain name, then override with "--domain=" or override entirely with
        --host.

    --prefix=<prefix>

        Optionally allows a directory/pattern to be inserted in between the http host
        specification and the /~user.

        This can also be specified by a MKURL_PREFIX environment variable.

    --suffix=<suffix>

        Optionally allows a directory/pattern to be inserted after the /~user.

        This can also be specified by a MKURL_SUFFIX environment variable.

    --home=<home-www-dir>

        Defaults to ~/www

        This can also be specified by a MKURL_HOME environment variable.
''' )

def is_arg( arg, prefix ):
	return arg.find( prefix ) == 0

def arg_value( arg ):
	return arg[ arg.find( "=" ) + 1 : ]

def error( arg ):
	print( "mkurl: %s" % arg )
	sys.exit( 1 )

def use( component ):
	if component != "": return "/" + component
	return ""

def base_url( ):
	if base != "": return base
	return "http://" + host + use( prefix ) + "/~" + os.getenv( "USER" ) + use( suffix )

def map_file( filename ):
	path = os.path.abspath( filename )
	url = requests.utils.requote_uri( base_url( ) + path )
	link = home + use( suffix ) + path
	if os.path.isdir( path ):
		raise Exception( "Directories cannot be shared, only files" )
	return ( path, url, link )

def create( filename ):
	path, url, link = map_file( filename )
	if not os.path.exists( link ):
		link_dir = os.path.dirname( link )
		os.makedirs( link_dir, exist_ok = True )
		os.symlink( path, link )
	return url

def handle_switch( arg ):
	global base, host, domain, prefix, suffix, home
	if is_arg( arg, "--url=" ): base = arg_value( arg )
	elif is_arg( arg, "--host=" ): host = arg_value( arg )
	elif is_arg( arg, "--domain=" ): domain = arg_value( arg )
	elif is_arg( arg, "--prefix=" ): prefix = arg_value( arg )
	elif is_arg( arg, "--suffix=" ): suffix = arg_value( arg )
	elif is_arg( arg, "--home=" ): home = arg_value( arg )
	else: error( "Unrecognised argument: %s" % arg )
