#!/usr/bin/env python3

import os
import sys
import subprocess

# Global map for properties
properties = { }
process = None
show_props = False

def error( msg ):
	print( "vml2dot: %s" % msg )
	sys.exit( 1 )

def get_string( remainder ):
	index = 1
	close = remainder[ 0 ]
	while index < len( remainder ) and remainder[ index ] != close:
		if remainder[ index ] == '\\': index += 1
		index += 1
	if index >= len( remainder ) or remainder[ index ] != close:
		raise Exception( "Cannot extract well formed string from %s" % remainder )
	index += 1
	return remainder[ 0 : index ]

def get_token( remainder ):
	index = 0
	while index < len( remainder ) and not remainder[ index ].isspace( ):
		if remainder[ index ] == "'" or remainder[ index ] == '"':
			index += len( get_string( remainder[ index : ] ) )
		else:
			index += 1
	return remainder[ 0 : index ]

def is_property( token ):
	equals = token.find( '=' )
	if equals < 0: return False
	if token[ 0 ] == '@': return True
	colon = token.find( ':' )
	if colon < 0: return True
	return colon > equals

def fetch( component ):
	global properties, process
	if component.find( "filter:" ) != 0: component = "input:" + component.replace( '\"', '' )
	if component in properties: return properties[ component ]
	if process is None:
		process = subprocess.Popen( [ 'vmlbatch', 'eval-stdin' ], stdin = subprocess.PIPE, stdout = subprocess.PIPE )
	props = { }
	process.stdin.write( ( "try $ \"%s\" prop-report-slots catch drop done cr\n" % component ).encode( ) )
	process.stdin.flush( )
	while True:
		line = process.stdout.readline( )
		if not line: break
		line = line.decode( "utf-8" ).strip( )
		if line == "": break
		key, value = line.split( "=", 1 )
		props[ key ] = value
	properties[ component ] = props
	return props

def handle_token( current, token ):
	if token[ 0 ] == '#': return current, token
	elif not is_property( token ): return token, token
	props = fetch( current )
	key, value = token.split( "=", 1 )
	if key in props.keys( ) and value == props[ key ]: token = ''
	return current, token

def create_key( keys, token ):
	if not token in keys:
		keys[ token ] = 0
	else:
		keys[ token ] += 1
	return token.replace( '\"', '' ) + str( keys[ token ] )

def slot_count( node, instance ):
	props = fetch( node[ 0 ] )
	slots = 0
	if "_slots_" in props: slots = int( props[ "_slots_" ] )
	if "slots" in props: slots = int( props[ "slots" ] )
	if "slots" in instance: slots = int( instance[ "slots" ] )
	return slots

def to_label( node, instance ):
	global show_props
	label = node[ 0 ]
	if label.find( "filter:" ) == 0: label = label.replace( "filter:", "", 1 )
	if label.find( ':' ) != label.rfind( ':' ): label = label.replace( ':', ':\\n' )
	if len( instance ) > 0 and show_props:
		label += "\\n"
		s = 0
		for k in instance:
			if k.find( "@vml_" ) == 0: continue
			prop = "%s=%s " % ( k, instance[ k ] )
			if len( prop ) > 30:
				prop = prop[ 0 : 27 ] + "..."
			label += prop
			s += len( prop )
			if s > 30:
				label += "\\n"
				s = 0
	label = label.replace( '\"', '\'' )
	return label

def dump_node( key, node, instance, output ):
	slots = slot_count( node, instance )
	extra = ""
	if slots == 0: extra = "shape=box rank=source color=lightblue2 style=filled"
	elif slots == 1: extra = "color=goldenrod2 style=filled"
	elif slots > 1: extra = "color=darkgreen style=filled"
	print( "{ node [label=\"%s\" %s] \"%s\"; }" % ( to_label( node, instance ), extra, key ), file = output )

def dump_nodes( nodes, instances, output ):
	for node in nodes:
		dump_node( node, nodes[ node ], instances[ node ], output )

def connect( mappings, stack, nodes, instances ):
	if len( stack ) > 0 and stack[ -1 ] not in mappings:
		tos = stack.pop( )
		mappings[ tos ] = [ ]
		slots = slot_count( nodes[ tos ], instances[ tos ] )
		for i in range( slots ):
			mappings[ tos ] = [ stack.pop( ) ] + mappings[ tos ]
		stack.append( tos )

def dump_connections( mappings, output ):
	for key in mappings:
		for item in mappings[ key ]:
			print( "\"%s\" -> \"%s\";" % ( key, item ), file = output )

def parse( input, output ):
	print( "digraph untitled {", file = output )
	print( "graph [bgcolor=lightgrey];", file = output )
	print( "rankdir=RL;", file = output )
	print( "ordering=in;", file = output )

	current = ''
	stack = [ ]
	keys = { }
	nodes = { }
	instances = { }
	mappings = { }
	variables = { }
	last = ''

	while True:
		line = input.readline( )
		if not line: break
		line = line.strip( )
		index = 0
		while index < len( line ):
			token = ''
			if line[ index ].isspace( ):
				index += 1
				continue
			elif line[ index ] == '#':
				break
			elif line[ index ] == "'" or line[ index ] == '"':
				token = get_string( line[ index : ] )
			else:
				token = get_token( line[ index : ] )
			index += len( token )
			current, result = handle_token( current, token )
			if current in [ "assign_variable", "use_variable", "wipe_variable" ]:
				last = current
			elif last != '':
				connect( mappings, stack, nodes, instances )
				if last == "assign_variable": variables[ result ] = stack[ -1 ]
				elif last == "use_variable": stack.append( variables[ result ] )
				elif last == "wipe_variable": variables.pop( result )
				last = ''
			elif current == result:
				connect( mappings, stack, nodes, instances )
				key = create_key( keys, current )
				stack.append( key )
				nodes[ key ] = [ result ]
				instances[ key ] = { }
			elif result != '':
				nodes[ key ].append( result )
				k, v = result.split( "=", 1 )
				instances[ key ][ k ] = v

	connect( mappings, stack, nodes, instances )
	dump_nodes( nodes, instances, output )
	dump_connections( mappings, output )
	print( "}", file = output )

def run( filename, output = sys.stdout, props = False ):
	global show_props
	show_props = props
	if filename == "-":
		parse( sys.stdin, output )
	elif os.path.exists( filename ):
		with open( filename ) as input:
			parse( input, output )
	return True
