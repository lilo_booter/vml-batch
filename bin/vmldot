#!/usr/bin/env python3

import gi
gi.require_version( 'Gtk', '3.0' )

from gi.repository import Gtk, GObject, Gdk, GLib
import xdot
import vml2dot
import sys
import os
import time
from io import StringIO

def help( ):
    print( '''
Usage:

    vmldot [ --show-props ] file

Description:

    This script renders a vml graph dump as an interactive dot graph.
''' )
    sys.exit( 0 )


class vml_dot_window( xdot.DotWindow ):

	def __init__( self, filename, show_props = False ):
		xdot.DotWindow.__init__( self )
		self.filename = filename
		self.show_props = show_props
		self.refresh( )
		self.connect( 'delete-event', Gtk.main_quit )
		if self.filename != '-':
			self.connect( 'key-press-event', self.key_press )
			GLib.timeout_add( 1000, self.timeout )

	def refresh( self ):
		if self.filename == '-': pass
		elif os.path.isfile( self.filename ): self.mtime = os.stat( self.filename )[ 8 ]
		else: return
		output = StringIO( )
		vml2dot.run( self.filename, output, props = self.show_props )
		self.set_dotcode( output.getvalue( ).encode( ) )

	def timeout( self ):
		if os.path.isfile( self.filename ):
			if hasattr( self, 'mtime' ):
				mtime = os.stat( self.filename )[ 8 ]
				if mtime > self.mtime: self.refresh( )
			else:
				self.refresh( )
		return True

	def key_press( self, widget, event ):
		if event.state & Gdk.ModifierType.CONTROL_MASK:
			if event.keyval == 32:
				self.show_props = not self.show_props
				self.refresh( )
				return True
		return False

def main( args ):
	show_props = False
	filename = ''

	for arg in args:
		if arg == '--help': help( )
		elif arg == '--show-props': show_props = True
		elif filename == '': filename = arg
		else: error( "Unrecognised argument " + arg )

	if filename == '' and not sys.stdin.isatty( ):
		window = vml_dot_window( "-", show_props )
	elif filename != '':
		window = vml_dot_window( filename, show_props )
	else:
		help( )

	Gtk.main( )

if __name__ == '__main__':
	main( sys.argv[ 1 : ] )
