# vml-batch

The vml-batch project provides a scripting tool for video and audio
playout and processing.

This tool is implemented using the [rpany] library.

## Usage Summary

Typical use is summarised as:

```
vmlbatch <graph> [ -- <store>* ]
```

To play files:

```
vmlbatch file.mp4
vmlbatch file.mp3
vmlbatch file.jpg
etc
```

To play many files:

```
vmlbatch *.mp4 depth filter:playlist slots=%s
```

To create clips and stitch them together:

```
vmlbatch file1.mp4 filter:clip in=500 out=1000 file2.mp4 filter:clip in=2000 out=2500 filter:playlist slots=2
```

And many more filters are available for video fx, conversion, encoding,
transcoding, remuxing, compositing, deinterlacing, audio mixing and
so on.

More details about the vml library itself to follow.

Alternatively, you can enter an interactive shell:

```
$ vmlshell
> file1.mp4
> filter:clip in=500 out=1000
> file2.mp4
> filter:clip in=2000 out=2500
> filter:playlist slots=2
> .
```

And to run the unit tests:

```
vmlbatch-test
```

## Graph

The primary purpose of **vmlbatch** is to parse and use a VML `graph`.

A VML graph consists of `inputs`, `filters` and `stores`.

`Inputs` are objects which provide a number of `frames` and can be configured
by way of `properties`. Typically, they are specified as URL's or file paths.

`Filters` are objects which connect to 1 or more inputs and can be configured
by way of `properties`. Typically, these are specified by strings of the form
`filter:<name>` where `<name>` indicates a registered VML filter.

`Stores` are objects which accept frames and provide playout, encode, analysis,
reporting and other functionality. With vmlbatch, there are two ways of
specifying stores - either as part of the graph, or as separated store objects.

To specify a store as part of a graph, we use the `store:` prefix, for example:

```
$ vmlbatch test: store:out.raw
```

which will simply produce a raw image dump of every frame from test:. The
resultant file can be played out as an input.

To specify a store as external from the graph, use an `--` argument to terminate
the graph, and specify the store without the store: prefix:

```
$ vmlbatch test: -- out.raw
```

Multiple stores can be specified.

`Properties` are `name=value` pairs and are typically defined in the VML
components themselves and proide a means to control their behaviour.

## Playout

During playout, the following key presses are honoured:

| Key   | Action                                                           |
|-------|------------------------------------------------------------------|
| esc   | close the window and exit                                        |
| 0     | return to the first frame                                        |
| j     | reverse direction - repeated use speeds up playout               |
| k     | toggles pause/play                                               |
| l     | forward direction - repeated use speeds up playout               |
| m     | reverse direction - repeated use slows down playout              |
| ,     | forward direction - repeated use slows down playout              |
| up    | backward 100 frames                                              |
| down  | forward 100 frames                                               |
| end   | go to last frame                                                 |
| g     | backward 1000 frames                                             |
| h     | forward 1000 frames                                              |
| left  | step 1 frame backward                                            |
| right | step 1 frame forward                                             |
| space | toggles pause/play                                               |
| f     | fullscreen                                                       |
| =     | grab audio when multiple players are running                     |

Additionally, if a playlist or compositor component is located in the graph
being played, the positions of each connected item are obtained and the
following keys can be used to navigate them:

| Key   | Action                                                           |
|-------|------------------------------------------------------------------|
| a     | go to the start of the previous item                             |
| s     | go to the start of the current item                              |
| d     | go to the start of the next item                                 |

By default, when no playlist or compositor is detected, a and s take you to
the start of the current item - d does nothing.

## General

The tool also provides a comprehensive scripting environment which allows the
use of the complete **rpany** library.

Like **rpany**, it also provides its own vocabularies which are defined below.

## Vocabularies

A `token` in **vmlbatch** is defined as one of:

* a `number` is passed directly to the rpany stack
* a `word` is evaluated and can be any word below or from the [rpany vocabs]
* `strings` which represent `local files` or `URLs` are converted to VML inputs
* `aml` dump files are acceped and deserialised
* Special VML inputs (such as colour:, silence: etc) are also accepted
* Filter specifications (of the form `filter:<name>`)
* Store specifications (of the form `store:<name>`)
* Property assignment (of the form `name=value`)

The `.` word has been overloaded and behaves differently depending on the contents
at the top of the stack.

* A graph without `store:` components is played in a preview: window
* A graph with `store:` components is iterated over until eof
* All other objects are displayed on stdout (using **rpany** coercion rules)

### `player`

These provide the simplest way to play a `graph`. A more flexible way is
provided by the `job` vocab describe below.

| Word             | Usage             | Result            | Comment                                       |
|------------------|-------------------|-------------------|-----------------------------------------------|
| `.`              | `<item> .`        |                   | Play graphs or output to stdout               |
| `player`         | `<graph> player`  |                   | Plays the graph and removes it from stack     |
| `preview`        | `<graph> preview` | `<graph>`         | Plays the graph and leaves it on the stack    |

The following can be used to control the initial playout state and should be
used prior to playing:

| Word                 | Usage                       | Comment                 |
|----------------------|-----------------------------|-------------------------|
| `player-speed`       | `<int> player-speed`        | Initial player speed    |
| `player-position`    | `<int> player-position`     | Initial player position |
| `player-pitch`       | `<double> player-pitch`     | Initial player pitch    |

Additionally the player store is specified by way of the `player-spec` word -
this can be rewritten to use different video and audio stores, or additional
stores such as SDI or other stores.

### `graphs`

This vocab provides a collection of means to inquire upon and modify graphs.

| Word             | Usage                          | Result            | Comment                                       |
|------------------|--------------------------------|-------------------|-----------------------------------------------|
| `describe`       | `<graph> describe`             | `<graph>`         | Shows the definition of the current graph     |
| `playlist`       | `<graph> .... <n> playlist`    | `<playlist>`      | Creates a playlist contain n graphs           |
| `length?`        | `<graph> length?`              | `<graph> <count>` | Puts the frame count of graph at top of stack |
| `slots?`         | `<graph> slots?`               | `<graph> <count>` | Puts the slot count of graph at top of stack  |
| `seekable?`      | `<graph> seekable?`            | `<graph> <bool>`  | Indicates if graph is seekable                |
| `distributable?` | `<graph> distributable?`       | `<graph> <int>`   | Shows distributable rule (0 is off)           |
| `sync`           | `<graph> <n> sync`             | `<graph>`         | Forces a sync on the graph                    |
| `slot`           | `<graph> <n> slot`             | `<graph> <graph>` | Puts slot n of graph at top of stack          |
| `connect`        | `<graph> <graph> <n> connect`  | `<graph>`         | Connects upper graph to lower graph at slot n |
| `decap`          | `<graph> decap`                | `<graph> ...`     | Removes filter at bottom of graph             |
| `commit`         | `<graph> commit`               | `<graph>`         | Commits current graph to stack                |
| `clone`          | `<graph> clone`                | `<graph> <graph>` | Creates a clone of the graph                  |

* `<playlist>` are a type of `<graph>`

### `job`

A `job` provides an alternative means to use a graph. By default, `job-start`
provides a player which doesn't block the shell which invoked it. If stores are
specified they simply render the graph to the stores also in a non-blocking way.

| Word             | Usage                          | Result           | Comment                                          |
|------------------|--------------------------------|------------------|--------------------------------------------------|
| `job-create`     | `<graph> job-create`           | `<job>`          | Creates a `<job>` which consumes `<graph>`       |
| `job-run`        | `<job> job-run`                |                  | Runs the `<job>` until completion if not started |
| `job-start`      | `<job> job-start`              | `<job>`          | Starts `<job>`                                   |
| `job-speed`      | `<job> <int> job-speed`        | `<job>`          | Sets the current speed of playout                |
| `job-pitch`      | `<job> <double> job-pitch`     | `<job>`          | Sets the current pitch of playout                |
| `job-seek`       | `<job> <int> job-seek`         | `<job>`          | Seeks to the requested frame                     |
| `job-speed?`     | `<job> job-speed?`             | `<job> <int>`    | Gets the current speed of playout                |
| `job-pitch?`     | `<job> job-pitch?`             | `<job> <double>` | Gets the current pitch of playout                |
| `job-position?`  | `<job> job-position?`          | `<job> <int>`    | Gets the current requested frame                 |
| `job-frames?`    | `<job> job-frames?`            | `<job> <int>`    | Gets the current number of frames                |

### `smart`

VML provides a lot of filters for compositing (using filter:compositor or others) -
this vocab provides a few easy to set up and potentially useful arrangements.

| Word             | Usage                            | Result       | Comment                                       |
|------------------|----------------------------------|--------------|-----------------------------------------------|
| `join`           | `<graph> <graph> join`           | `<playlist>` | Creates a playlist containing both graphs     |
| `side-by-side`   | `<graph> <graph> side-by-side`   | `<mosaic>`   | Side by side mosaic                           |
| `over-and-under` | `<graph> <graph> over-and-under` | `<mosaic>`   | Over and under mosaic                         |
| `smart-playlist` | `<graph> .... <n> smart-playlist`| `<playlist>` | Smart playlist                                |
| `smart-settings` | `<smart> smart-settings`         | `<graph>`    | Smart settings                                |

* `<playlist>` and `<mosaic>` are types of `<graph>`

### `normalise`

At the point of playout, a number of filters are automatically attached to the
graph to ensure compatability with the default player. These are not used if
`store:` components are present in the graph.

These words are typically not invoked directly, but can be modified as required
prior to playout

| Word               | Comment                                                    |
|--------------------|------------------------------------------------------------|
| `n-thread`         | Attaches a read ahead thread for smoother playout          |
| `n-visualise`      | Provides visualisation of audio if no video present        |
| `n-resample`       | Resamples the audio to 48khz and changes to stereo playout |
| `n-conform`        | Ensures that we have video and audio                       |
| `n-deinterlace`    | Deinterlaces the video                                     |
| `n-pitch`          | Introduces a pitch filter for audio speed up               |

The normalising filters used (with the exception of `thread`) are specified by
the `normalise` word which is defined as:

```
: normalise n-visualise n-resample n-conform n-deinterlace n-pitch ;
```

### `frames`

A `graph` provides a number of `frames`. A `frame` typically holds an image, the
associated audio and a collection of metadata. It can also provide some forms of
compressed media.

| Word          | Usage                  | Result                      | Comment                                   |
|---------------|------------------------|-----------------------------|-------------------------------------------|
| `fetch`       | `<graph> <n> fetch`    | `<graph> <frame>`           | Extracts frame n from the graph           |
| `frame?`      | `<item> frame?`        | `<bool>`                    | Indicates item is a frame                 |
| `has-image?`  | `<frame> has-image?`   | `<frame> <bool>`            | Indicates if frame provides an image      |
| `has-audio?`  | `<frame> has-audio?`   | `<frame> <bool>`            | Indicates if frame provides an audio      |
| `has-alpha?`  | `<frame> has-alpha?`   | `<frame> <bool>`            | Indicates if frame provides an alpha mask |
| `frame-fps`   | `<frame> frame-fps`    | `<frame> <num>:<den>`       | Provides fps as a rational                |
| `frame-dim`   | `<frame> frame-dim`    | `<frame> <w> <h>`           | Provides dimensions of frame              |
| `frame-sar`   | `<frame> frame-sar`    | `<frame> <num>:<den>`       | Provides fps as a rational                |
| `frame-audio` | `<frame> frame-audio`  | `<frame> <rate> <channels>` | Provides audio info                       |

### `timecodes`

Provides a timecode calculator.

| Word             | Usage                   | Result  | Comment                                                            |
|------------------|-------------------------|---------|--------------------------------------------------------------------|
| `tc-set-fps`     | `<fps> tc-set-fps`      |         | Assigns tc-fps                                                     |
| `tc`             | `tc <int>`              | `<tc>`  | Converts int to a non-dropframe timecode                           |
| `tc-df`          | `tc-df <int>`           | `<tc>`  | Converts int to a dropframe timecode                               |
| `tc`             | `tc <str>`              | `<tc>`  | Converts string to a timecode (dropframe implied by string)        |
| `to-tc`          | `<int> to-tc`           | `<tc>`  | Converts int at tos to a non-dropframe timecode                    |
| `to-tc-df`       | `<int> to-tc-df`        | `<tc>`  | Converts int at tos to a dropframe timecode                        |
| `to-tc`          | `<str> to-tc`           | `<tc>`  | Converts string at tos to a timecode (dropframe implied by string) |
| `tc-to-position` | `<tc> tc-to-position`   | `<int>` | Converts timecode to integer position                              |
| `tc-add`         | `<tc> <tc> tc-add`      | `<tc>`  | Adds two timecodes                                                 |
| `tc-subtract`    | `<tc> <tc> tc-subtract` | `<tc>`  | Subtracts two timecodes                                            |
| `tc-fps`         | `tc-fps`                | `<fps>` | Puts current fps at the top of the stack                           |

Example of use:

```
> tc 500
ERROR: rpany::stack_exception: FPS not specified - use <fps> tc-set-fps
> dump
0: '500'
> 25:1 tc-set-fps
> timecode dump
0: '00:00:20:00'
> tc 100 dump
1: '00:00:20:00'
0: '00:00:04:00'
> timecode-add dump
0: '00:00:24:00'
> tc 00:00:01:00 dump
1: '00:00:24:00'
0: '00:00:01:00'
> tc-add dump
0: 00:00:25:00
> tc-to-position .
625
```

### `properties`

All `inputs`, `filters`, `stores` and `frames` have `properties`. These are a
collection of name/value pairs which are typically used to modify the behaviour
of `graph` components.

| Word          | Usage                        | Result            | Comment                                        |
|---------------|------------------------------|-------------------|------------------------------------------------|
| `prop-see`    | `<props> prop-see`           | `<props>`         | Reports property settings of graph/frame/store |
| `prop-exists` | `<props> <name> prop-exists` | `<props> <bool>`  | Indicates if prop exists                       |
| `prop-query`  | `<props> <name> prop-query`  | `<props> <value>` | Obtains value of property                      |

### `glob`

Provides glob functionality for shell style wild card expansion of file names.

| Word        | Usage                   | Result      | Comment                                        |
|-------------|-------------------------|-------------|------------------------------------------------|
| `glob`      | `<string> glob`         | `... <int>` | Replaces string with 'glob' expanded list      |
| `glob-list` | `... <int> glob-list`   | `... <int>` | Replaces string list with 'glob' expanded list |

### `debug`

| Word         | Usage                          | Result            | Comment                                       |
|--------------|--------------------------------|-------------------|-----------------------------------------------|
| `log-level`  | `<n> log-level`                |                   | Sets the log level                            |

[rpany]: https://gitlab.com/lilo_booter/rpany/blob/master/README.md
[rpany vocabs]: https://gitlab.com/lilo_booter/rpany/blob/master/README.md#vocabularies
