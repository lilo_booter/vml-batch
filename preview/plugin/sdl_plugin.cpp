// sdl - An sdl plugin to ml.
//
// Copyright (C) 2018 VizRt
// Released under the LGPL.
//
// #store:vmlbatch-video:
//
// Provides a store for video playout using the vmlbatch-preview library.
//
// This plugin provides a minimalist store which defines the properties which
// the real sdl video surface will use.

#include <vmlbatch_preview.hpp>
#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <SDL.h>

namespace pl = olib::openpluginlib;
namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;

using boost::uint8_t;
using boost::uint32_t;
using boost::uint64_t;

namespace vmlbatch {

class ML_PLUGIN_DECLSPEC vmlbatch_video : public ml::store_type, public std::enable_shared_from_this< vmlbatch_video >
{
	public:
		vmlbatch_video( )
		: prop_winid_( pl::pcos::key::from_string( "winid" ) )
		, prop_scaling_( pl::pcos::key::from_string( "scaling" ) )
		, prop_width_( pl::pcos::key::from_string( "width" ) )
		, prop_height_( pl::pcos::key::from_string( "height" ) )
		, prop_x_( pl::pcos::key::from_string( "x" ) )
		, prop_y_( pl::pcos::key::from_string( "y" ) )
		, prop_keydown_( pl::pcos::key::from_string( "keydown" ) )
		, prop_keymod_( pl::pcos::key::from_string( "keymod" ) )
		, prop_full_( pl::pcos::key::from_string( "fullscreen" ) )
		, prop_native_( pl::pcos::key::from_string( "native" ) )
		, prop_vsync_( pl::pcos::key::from_string( "vsync" ) )
		{
			// Provide a means to control the monitor to use
			int position = SDL_WINDOWPOS_UNDEFINED;
			if ( getenv( "VML_MONITOR" ) )
			{
				int monitor = atoi( getenv( "VML_MONITOR" ) );
				position = SDL_WINDOWPOS_CENTERED_DISPLAY( monitor );
			}

			properties( ).append( prop_winid_ = uint64_t( 0 ) );
			properties( ).append( prop_scaling_ = 1 );
			properties( ).append( prop_width_ = 640 );
			properties( ).append( prop_height_ = 480 );
			properties( ).append( prop_x_ = position );
			properties( ).append( prop_y_ = position );
			properties( ).append( prop_keydown_ = 0 );
			properties( ).append( prop_keymod_ = 0 );
			properties( ).append( prop_full_ = 0 );
			properties( ).append( prop_native_ = 0.0 );
			properties( ).append( prop_vsync_ = 1 );
		}

		virtual ~vmlbatch_video( )
		{
			vmlbatch::schedule( vmlbatch::task_destroy{ this } );
		}

		virtual bool init( )
		{
			return vmlbatch::schedule( vmlbatch::task_create{ shared_from_this( ) } );
		}

		virtual bool push( ml::frame_type_ptr frame )
		{
			return vmlbatch::schedule( vmlbatch::task_display{ shared_from_this( ), vmlbatch::frame_type_ptr( frame.release( ) ) } );
		}

		virtual void complete( )
		{
		}

		virtual ml::frame_type_ptr flush( )
		{
			return ml::frame_type_ptr( );
		}

		pl::pcos::property prop_winid_;
		pl::pcos::property prop_scaling_;
		pl::pcos::property prop_width_;
		pl::pcos::property prop_height_;
		pl::pcos::property prop_x_;
		pl::pcos::property prop_y_;
		pl::pcos::property prop_keydown_;
		pl::pcos::property prop_keymod_;
		pl::pcos::property prop_full_;
		pl::pcos::property prop_native_;
		pl::pcos::property prop_vsync_;
};

//
// Plugin object
//

class ML_PLUGIN_DECLSPEC sdl_plugin : public ml::openmedialib_plugin
{
public:
	virtual ml::input_type_ptr input( const ml::io::request &request )
	{
		return ml::input_type_ptr( );
	}

	virtual ml::store_type_ptr store( const ml::io::request &request, const ml::frame_type_ptr & )
	{
		return ml::store_type_ptr( new vmlbatch_video( ) );
	}

	virtual ml::filter_type_ptr filter( const std::wstring & )
	{
		return ml::filter_type_ptr( );
	}
};

}

//
// Access methods for openpluginlib
//

extern "C"
{
	ML_PLUGIN_DECLSPEC bool openplugin_init( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_uninit( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_create_plugin( const char*, pl::openplugin** plug )
	{
		*plug = new vmlbatch::sdl_plugin;
		return true;
	}

	ML_PLUGIN_DECLSPEC void openplugin_destroy_plugin( pl::openplugin* plug )
	{
		delete static_cast< vmlbatch::sdl_plugin * >( plug );
	}
}
