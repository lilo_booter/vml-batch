// vml-batch preview library
//
// Copyright (C) 2020 VizRT
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// Reworked from the VML sdl_video: plugin to allow video presentation to be
// handled exlusively on the main thread of the vmlbatch process.

#include "vmlbatch_preview.hpp"
#include "keysym_mapping.hpp"

#include <SDL.h>

#include <deque>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include <boost/any.hpp>

namespace vmlbatch {

using namespace std::chrono;

// Error checking for SDL functions that return an error code or a pointer, respectively.

#define ARENFORCE_SDL( desc, sdl_call ) \
	ARENFORCE_MSG( 1 == 1 + ( sdl_call ), "%s failed. SDL error message: %s" ) ( desc )( SDL_GetError() ); \

#define ARENFORCE_SDL_PTR( desc, sdl_call ) \
	ARENFORCE_MSG( ( void * )0 != ( sdl_call ), "%s failed. SDL error message: %s" ) ( desc )( SDL_GetError() );

// If the incoming image is not in one of the natively supported formats we need to convert.
static const ml::image::MLPixelFormat fallback_ml_pf_ = ml::image::ML_PIX_FMT_YUV420P;

// Maps from ML to SDL pixel format
static uint32_t ml_to_sdl_pf( ml::image::MLPixelFormat ml_pf )
{
	switch( ml_pf )
	{
		case ml::image::ML_PIX_FMT_YUV420P: return SDL_PIXELFORMAT_YV12;
		case ml::image::ML_PIX_FMT_R8G8B8A8: return SDL_PIXELFORMAT_ABGR8888;
		case ml::image::ML_PIX_FMT_B8G8R8A8: return SDL_PIXELFORMAT_ARGB8888;
		case ml::image::ML_PIX_FMT_A8R8G8B8: return SDL_PIXELFORMAT_BGRA8888;
		case ml::image::ML_PIX_FMT_A8B8G8R8: return SDL_PIXELFORMAT_RGBA8888;
		case ml::image::ML_PIX_FMT_R8G8B808: return SDL_PIXELFORMAT_ABGR8888;
		case ml::image::ML_PIX_FMT_B8G8R808: return SDL_PIXELFORMAT_ARGB8888;
		case ml::image::ML_PIX_FMT_08R8G8B8: return SDL_PIXELFORMAT_BGRX8888;
		case ml::image::ML_PIX_FMT_08B8G8R8: return SDL_PIXELFORMAT_RGBX8888;
		case ml::image::ML_PIX_FMT_YUV422:  return SDL_PIXELFORMAT_YUY2;
		case ml::image::ML_PIX_FMT_UYV422:  return SDL_PIXELFORMAT_UYVY;
		default: return SDL_PIXELFORMAT_UNKNOWN;
	}
}

// Global event queue mutex and window queues
typedef std::unique_lock< std::mutex > scoped_lock;
static std::mutex _sdl_event_queue_mutex;
typedef std::deque< SDL_Event > event_queue;
std::map< Uint32, event_queue > event_map;

static void close_video( )
{
	SDL_QuitSubSystem( SDL_INIT_VIDEO );
}

static void init_video( )
{
	static bool initialised = false;
	if ( !initialised )
	{
		ARENFORCE_SDL( "SDL_Init", SDL_Init( SDL_INIT_VIDEO ) );
		atexit( close_video );
		initialised = true;
	}
}

class sdl_display
{
	public:
		sdl_display( ml::store_type_ptr store )
		: store_( store )
		, prop_winid_( store->properties( ).get_property_with_string( "winid" ) )
		, prop_scaling_( store->properties( ).get_property_with_string( "scaling" ) )
		, prop_width_( store->properties( ).get_property_with_string( "width" ) )
		, prop_height_( store->properties( ).get_property_with_string( "height" ) )
		, prop_x_( store->properties( ).get_property_with_string( "x" ) )
		, prop_y_( store->properties( ).get_property_with_string( "y" ) )
		, prop_keydown_( store->properties( ).get_property_with_string( "keydown" ) )
		, prop_keymod_( store->properties( ).get_property_with_string( "keymod" ) )
		, prop_full_( store->properties( ).get_property_with_string( "fullscreen" ) )
		, prop_native_( store->properties( ).get_property_with_string( "native" ) )
		, prop_vsync_( store->properties( ).get_property_with_string( "vsync" ) )
		, rescaler_( new ml::image::rescale_object( ) )
		, last_mouse_move_( steady_clock::now( ) )
		{
			init_video( );

			const uint64_t winid = prop_winid_.value< uint64_t >( );
			if( winid == 0 )
			{
				ARENFORCE_SDL_PTR( "SDL_CreateWindow",
					sdl_window_ = SDL_CreateWindow(
						"vmlbatch",
						prop_x_.value< int >( ),
						prop_y_.value< int >( ),
						prop_width_.value< int >( ),
						prop_height_.value< int >( ),
						SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI ) );
			}
			else
			{
				const void *winid_ptr = reinterpret_cast< const void * >( winid );
				ARENFORCE_SDL_PTR( "SDL_CreateWindowFrom", sdl_window_ = SDL_CreateWindowFrom( winid_ptr ) );
			}

			int flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE;
			if ( prop_vsync_.value< int >( ) )
				flags |= SDL_RENDERER_PRESENTVSYNC;

			ARENFORCE_SDL_PTR( "SDL_CreateRenderer", sdl_renderer_ = SDL_CreateRenderer( sdl_window_, -1, flags ) );

			const char *scaling_strs[] = { "0", "1", "2" };
			const int arg = prop_scaling_.value< int >( );

			ARENFORCE_MSG( arg >= 0 && arg <= 2, "Invalid value for scaling property (0-2 was expected): %1%" ) ( arg );

			if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, scaling_strs[ arg ] ) )
			{
				ARLOG_WARN( "Failed to set scaling mode to %1% in sdl_video store" ) ( scaling_strs[ arg ] );
			}

			current_width_ = prop_width_.value< int >( );
			current_height_ = prop_height_.value< int >( );

			SDL_SetYUVConversionMode( SDL_YUV_CONVERSION_AUTOMATIC );
		}

		virtual ~sdl_display( )
		{
			if( sdl_texture_ )
				SDL_DestroyTexture( sdl_texture_ );
			if( sdl_renderer_ )
				SDL_DestroyRenderer( sdl_renderer_ );
			if( sdl_window_ )
			{
				// Remove the event queue index associated to this closing window.
				scoped_lock lock( _sdl_event_queue_mutex );
				event_map.erase( SDL_GetWindowID( sdl_window_ ) );
				SDL_DestroyWindow( sdl_window_ );
			}
		}

		//Returns false if a quit event was raised, otherwise true.
		bool handle_window_events( )
		{
			//If we have a keyup event stored from last time, reset
			//the keydown property, since the client has now had time
			//to handle the keypress.
			if ( have_stored_keyup_event_ )
			{
				prop_keydown_ = 0;
				have_stored_keyup_event_ = false;
			}

			// We want to lock the event queue mutex
			scoped_lock lock( _sdl_event_queue_mutex );

			// We'll poll the event queue for the specific events we want and retain
			// in a separate queue for each active window
			SDL_Event event;
			while ( SDL_PollEvent( &event ) )
				if ( event.type == SDL_WINDOWEVENT || event.type == SDL_KEYUP || event.type == SDL_KEYDOWN || event.type == SDL_MOUSEMOTION )
					event_map[ event.window.windowID ].push_back( event );

			// Obtain the event queue for this window
			event_queue &queue = event_map[ SDL_GetWindowID( sdl_window_ ) ];

			// Iterate through each event, removing them as we go.
			auto iter = queue.begin( );

			while ( iter != queue.end( ) )
			{
				event = *iter;
				iter = queue.erase( iter );

				switch( event.type )
				{
					case SDL_WINDOWEVENT:
						if( event.window.event == SDL_WINDOWEVENT_RESIZED )
						{
							current_width_ = event.window.data1;
							current_height_ = event.window.data2;
							prop_width_ = current_width_;
							prop_height_ = current_height_;
						}
						else if ( event.window.event ==  SDL_WINDOWEVENT_CLOSE )
						{
							return false;
						}
						break;

					case SDL_KEYDOWN:
						prop_keymod_ = sdl_keymod_to_ml_keymod( SDL_Keymod( event.key.keysym.mod ) );
						prop_keydown_ = sdl_keysym_to_ml_keysym( event.key.keysym.sym );
						have_stored_keyup_event_ = false;
						break;

					case SDL_KEYUP:
						prop_keymod_ = sdl_keymod_to_ml_keymod( SDL_Keymod( event.key.keysym.mod ) );
						//We record that the key up event happened here, but do not reset
						//prop_keydown until next time. This is done so that we don't lose
						//keypress event for keys which were both pressed and released in
						//the time since the last push to the store.
						have_stored_keyup_event_ = true;
						break;

					case SDL_MOUSEMOTION:
						last_mouse_move_ = steady_clock::now( );
						SDL_ShowCursor( SDL_ENABLE );
						break;

					default:
						break;
				}
			}

			if ( steady_clock::now( ) - last_mouse_move_ > seconds( 2 ) )
				SDL_ShowCursor( SDL_DISABLE );

			return true;
		}

		bool display( frame_type_ptr &frame )
		{
			// NOTE: This method should always succeed unless there is an error detected
			// in the underlying SDL library (indicated by way of an exception) or the
			// user closes the window, resulting in return false

			SDL_DisplayMode display_mode;
			ARENFORCE( SDL_GetCurrentDisplayMode( 0, &display_mode ) == 0 );
			if( display_mode.refresh_rate != last_refresh_rate_ )
			{
				if( last_refresh_rate_ == 0 )
				{
					ARLOG_INFO( "Monitor refresh rate is %1% Hz" )( display_mode.refresh_rate );
				}
				else
				{
					ARLOG_INFO( "Monitor refresh rate changed from %1% Hz to %2% Hz" )( last_refresh_rate_ )( display_mode.refresh_rate );
				}

				last_refresh_rate_ = display_mode.refresh_rate;
			}

			// Always handle events
			if( !handle_window_events( ) )
				return false;

			// Check that we have an image to work with
			const bool frame_has_image = frame != 0 && frame->get_image( ) != 0;
			if ( !frame_has_image && last_image_ == 0 )
				return true;

			// We will definitely select an image here
			const ml::image_type_ptr &selection = frame_has_image ? frame->get_image( ) : last_image_;
			ml::image_type_ptr img = selection->shallow( );

			// We'll pick up the current frames sar if we're using the image from there
			if ( frame != 0 && selection != last_image_ )
				frame->get_sar( last_sar_num_, last_sar_den_ );

			// Check that we have something in the image...
			if ( img->width( ) * img->height( ) == 0 || img->width( ) < 4 )
				return true;

			// Frame pixel format not natively supported by SDL - must convert
			if( ml_to_sdl_pf( img->ml_pixel_format() ) == SDL_PIXELFORMAT_UNKNOWN )
			{
				ARLOG_DEBUG2("Pixelformat %1% not supported. Converting to %2%") ( ml::image::MLPF_to_string( img->ml_pixel_format() ) ) ( ml::image::MLPF_to_string( fallback_ml_pf_ ) );
				ml::image::convert( rescaler_, img, ml::image::MLPF_to_string( fallback_ml_pf_ ) );
			}

			if ( prop_native_.value< double >( ) > 0.0 )
			{
				prop_width_ = int( prop_native_.value< double >( ) * img->width( ) * last_sar_num_ / last_sar_den_ );
				prop_height_ = int( prop_native_.value< double >( ) * img->height( ) );
				prop_native_ = 0.0;
			}

			const bool needs_resize =
				current_width_ != prop_width_.value< int >( ) ||
				current_height_ != prop_height_.value< int >( ) ||
				current_full_ != prop_full_.value< int >( );

			if ( needs_resize )
			{
				current_full_ = prop_full_.value< int >( );
				if ( current_full_ != 0 )
				{
					SDL_SetWindowFullscreen( sdl_window_, SDL_WINDOW_FULLSCREEN_DESKTOP );
				}
				else
				{
					SDL_SetWindowFullscreen( sdl_window_, 0 );
					SDL_SetWindowSize( sdl_window_, prop_width_.value< int >( ), prop_height_.value< int >( ) );
				}
				SDL_GetWindowSize( sdl_window_, &current_width_, &current_height_ );
			}

			SDL_Texture *texture = get_texture( img );
			if( img->plane_count() == 3 )
			{
				ARENFORCE_SDL( "SDL_UpdateYUVTexture",
					SDL_UpdateYUVTexture( sdl_texture_, NULL,
						(const uint8_t *)img->ptr( 0 ), img->pitch( 0 ),
						(const uint8_t *)img->ptr( 1 ), img->pitch( 1 ),
						(const uint8_t *)img->ptr( 2 ), img->pitch( 2 ) ) );
			}
			else
			{
				ARENFORCE_SDL( "SDL_UpdateTexture",
					SDL_UpdateTexture( sdl_texture_, NULL, (const uint8_t *) img->ptr( 0 ), img->pitch( 0 ) ) );
			}

			SDL_Rect target_rect;
			calculate_target_rect( target_rect, img, last_sar_num_, last_sar_den_ );
			ARENFORCE_SDL( "SDL_RenderClear", SDL_RenderClear( sdl_renderer_ ) );
			// SDL2 does not have pixel formats equivalent to RGB0 and BGR0, so it's mapped like RGBA and BGRA
			// We need to set blend mode to none in order to ignore the alpha channel and display the image.
			if ( img->ml_pixel_format() == ml::image::ML_PIX_FMT_R8G8B808 || img->ml_pixel_format() == ml::image::ML_PIX_FMT_B8G8R808 )
			{
				ARENFORCE_SDL( "SDL_SetTextureBlendMode", SDL_SetTextureBlendMode( sdl_texture_, SDL_BLENDMODE_NONE ) );
			}
			else
			{
				ARENFORCE_SDL( "SDL_SetTextureBlendMode", SDL_SetTextureBlendMode( sdl_texture_, SDL_BLENDMODE_BLEND ) );
			}
			ARENFORCE_SDL( "SDL_RenderCopy", SDL_RenderCopy( sdl_renderer_, texture, NULL, &target_rect ) );

			SDL_RenderPresent( sdl_renderer_ );

			last_image_ = std::move( img );

			return true;
		}

		void calculate_target_rect( SDL_Rect &rect, const ml::image_type_ptr &image, int sar_num, int sar_den )
		{
			int w, h;
			SDL_GL_GetDrawableSize( sdl_window_, &w, &h );

			double this_aspect = double( w ) / double( h );
			double frame_aspect = ( double( image->width( ) * sar_num ) / sar_den ) / image->height( );

			int dw = int( frame_aspect / this_aspect * w );
			int dh = h;
			if ( dw > w )
			{
				dw = w;
				dh = int( this_aspect / frame_aspect * h );
			}

			int dx = ( w - dw ) / 2;
			int dy = ( h - dh ) / 2;

			rect.x = dx;
			rect.y = dy;
			rect.w = dw;
			rect.h = dh;
		}

		SDL_Texture *get_texture( const ml::image_type_ptr &img )
		{
			const uint32_t texture_pf = ml_to_sdl_pf( img->ml_pixel_format() );
			ARENFORCE_MSG( texture_pf != SDL_PIXELFORMAT_UNKNOWN, "This is a bug. (Encoutered pf '%1%', which cannot be mapped to SDL)" ) ( ml::image::MLPF_to_string ( img->ml_pixel_format( ) ) );
			const int texture_width = img->width( );
			const int texture_height = img->height( );

			bool recreate_texture = !sdl_texture_;
			if( sdl_texture_ )
			{
				uint32_t old_sdl_pf;
				int dummy, old_w, old_h;
				ARENFORCE_SDL( "SDL_QueryTexture", SDL_QueryTexture( sdl_texture_, &old_sdl_pf, &dummy, &old_w, &old_h ) );
				recreate_texture = texture_pf != old_sdl_pf || texture_width != old_w || texture_height != old_h;
			}

			if ( recreate_texture )
			{
				if( sdl_texture_ )
					SDL_DestroyTexture( sdl_texture_ );
				ARENFORCE_SDL_PTR( "SDL_CreateTexture", sdl_texture_ = SDL_CreateTexture( sdl_renderer_, texture_pf, SDL_TEXTUREACCESS_STREAMING, texture_width, texture_height ) );
			}
			return sdl_texture_;
		}

	private:
		std::weak_ptr< ml::store_type > store_;

		pl::pcos::property prop_winid_;
		pl::pcos::property prop_scaling_;
		pl::pcos::property prop_width_;
		pl::pcos::property prop_height_;
		pl::pcos::property prop_x_;
		pl::pcos::property prop_y_;
		pl::pcos::property prop_keydown_;
		pl::pcos::property prop_keymod_;
		pl::pcos::property prop_full_;
		pl::pcos::property prop_native_;
		pl::pcos::property prop_vsync_;

		SDL_Window *sdl_window_ = 0;
		SDL_Renderer *sdl_renderer_ = 0;
		SDL_Texture *sdl_texture_ = 0;

		int current_width_ = 640;
		int current_height_ = 480;
		int current_full_ = 0;

		int last_sar_num_ = 1;
		int last_sar_den_ = 1;
		bool have_stored_keyup_event_ = false;
		ml::image_type_ptr last_image_;
		ml::rescale_object_ptr rescaler_;
		int last_refresh_rate_ = 0;
		steady_clock::time_point last_mouse_move_;
};

typedef std::shared_ptr< sdl_display > sdl_display_ptr;

// All state is protected by the mutex
static std::mutex mutex;

// Holds the scheduled tasks
static std::deque< boost::any > tasks;

// Signalled when a new task is scheduled
static std::condition_variable condition_in;

// Signalled when a task has been evaluated
static std::condition_variable condition_out;

// Counts the number of tasks requested
static int64_t tasks_in = 0;

// Counts the number of tasks evaluated
static int64_t tasks_out = 0;

// Maps vmlbatch sdl_video: stores to display objects
static std::map< ml::store_type *, sdl_display_ptr > stores;

// Relays success or failure of each scheduled task
static std::map< int64_t, bool > status;

bool schedule( boost::any any )
{
	// Lock access to the tasks
	std::unique_lock< std::mutex > lock( mutex );

	// Keep a note of outr task id
	int64_t id = ++ tasks_in;

	// Append requested task and notify
	tasks.push_back( std::move( any ) );
	condition_in.notify_one( );

	// Block until task complete
	while( id > tasks_out )
		condition_out.wait( lock );

	// Obtain status if available
	const auto iter = status.find( id );
	const auto found = iter != status.end( );
	const auto result = found ? iter->second : false;
	if ( found ) status.erase( iter );

	return result;
}

int process( )
{
	while( true )
	{
		// Lock access to the tasks
		std::unique_lock< std::mutex > lock( mutex );

		// Block until a task is scheduled
		while( tasks.empty( ) )
			condition_in.wait( lock );

		// Obtain the task from the event queue
		boost::any any = std::move( tasks.front( ) );
		tasks.pop_front( );

		// Determine the type of the task and evaluate
		if ( any.type( ) == typeid( task_exit ) )
		{
			tasks_out = std::numeric_limits< int64_t >( ).max( );
			condition_out.notify_all( );
			return boost::any_cast< task_exit >( any ).result;
		}

		// We'll relay the success or failure of the request through the status map
		bool result = true;

		try
		{
			if ( any.type( ) == typeid( task_create ) )
			{
				auto task = boost::any_cast< task_create >( any );
				stores[ task.store.get( ) ] = sdl_display_ptr( new sdl_display( task.store ) );
			}
			else if ( any.type( ) == typeid( task_display ) )
			{
				auto task = boost::any_cast< task_display >( any );
				auto iter = stores.find( task.store.get( ) );
				ARENFORCE_MSG( iter != stores.end( ), "Unable to find an sdl_display for the store requested" );
				result = iter->second->display( task.frame );
			}
			else if ( any.type( ) == typeid( task_destroy ) )
			{
				auto task = boost::any_cast< task_destroy >( any );
				auto iter = stores.find( task.store );
				if ( iter != stores.end( ) ) stores.erase( iter );
			}
		}
		catch( ... )
		{
			result = false;
		}

		// Relay result to scheduler
		status[ ++ tasks_out ] = result;
		condition_out.notify_all( );
	}

	return 0;
}

}
