#ifndef SDL_OPEN_GL_STORE_KEYSYM_MAPPING_H_
#define SDL_OPEN_GL_STORE_KEYSYM_MAPPING_H_

#include <SDL_keycode.h>
#include <openmedialib/ml/keysym.hpp>

namespace ml = olib::openmedialib::ml;

int sdl_keysym_to_ml_keysym( SDL_Keycode sdl_keysym )
{
	switch ( sdl_keysym )
	{
		case int( SDLK_ESCAPE ) : return ml::keysym::esc;
		case int( SDLK_END    ) : return ml::keysym::end;
		case int( SDLK_HOME   ) : return ml::keysym::home;
		case int( SDLK_UP     ) : return ml::keysym::up;
		case int( SDLK_DOWN   ) : return ml::keysym::down;
		case int( SDLK_LEFT   ) : return ml::keysym::left;
		case int( SDLK_RIGHT  ) : return ml::keysym::right;
	}

	return sdl_keysym;
}

int sdl_keymod_to_ml_keymod( SDL_Keymod SDLMod_value )
{
	int ret = 0;
	int sdl_keymod( SDLMod_value );

	if ( 0 != (sdl_keymod & KMOD_NONE  )  ) { ret |= ml::keymod::none;   sdl_keymod &= ~KMOD_NONE  ; }
	if ( 0 != (sdl_keymod & KMOD_LSHIFT)  ) { ret |= ml::keymod::lshift; sdl_keymod &= ~KMOD_LSHIFT; }
	if ( 0 != (sdl_keymod & KMOD_RSHIFT)  ) { ret |= ml::keymod::rshift; sdl_keymod &= ~KMOD_RSHIFT; }
	if ( 0 != (sdl_keymod & KMOD_LCTRL )  ) { ret |= ml::keymod::lctrl;  sdl_keymod &= ~KMOD_LCTRL ; }
	if ( 0 != (sdl_keymod & KMOD_RCTRL )  ) { ret |= ml::keymod::rctrl;  sdl_keymod &= ~KMOD_RCTRL ; }
	if ( 0 != (sdl_keymod & KMOD_LALT  )  ) { ret |= ml::keymod::lalt;   sdl_keymod &= ~KMOD_LALT  ; }
	if ( 0 != (sdl_keymod & KMOD_RALT  )  ) { ret |= ml::keymod::ralt;   sdl_keymod &= ~KMOD_RALT  ; }
#if ( SDL_MAJOR_VERSION == 1 && SDL_MINOR_VERSION < 3 )
	if ( 0 != (sdl_keymod & KMOD_LMETA )  ) { ret |= ml::keymod::lgui;   sdl_keymod &= ~KMOD_LMETA ; }
	if ( 0 != (sdl_keymod & KMOD_RMETA )  ) { ret |= ml::keymod::rgui;   sdl_keymod &= ~KMOD_RMETA ; }
#else
	if ( 0 != (sdl_keymod & KMOD_LGUI  )  ) { ret |= ml::keymod::lgui;   sdl_keymod &= ~KMOD_LGUI  ; }
	if ( 0 != (sdl_keymod & KMOD_RGUI  )  ) { ret |= ml::keymod::rgui;   sdl_keymod &= ~KMOD_RGUI  ; }
#endif
	if ( 0 != (sdl_keymod & KMOD_NUM   )  ) { ret |= ml::keymod::num;    sdl_keymod &= ~KMOD_NUM   ; }
	if ( 0 != (sdl_keymod & KMOD_CAPS  )  ) { ret |= ml::keymod::caps;   sdl_keymod &= ~KMOD_CAPS  ; }
	if ( 0 != (sdl_keymod & KMOD_MODE  )  ) { ret |= ml::keymod::mode;   sdl_keymod &= ~KMOD_MODE  ; }

	ARENFORCE_MSG( 0 == sdl_keymod, "Unknown SDLMod %d" )( int( sdl_keymod ) );
	return ret;
}

#endif


