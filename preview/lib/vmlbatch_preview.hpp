// vml-batch preview library
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// Provides a means to ensure video presentation can be handled exclusively
// on the main thread of the vmlbatch process.

#pragma once

#include <openmedialib/ml/openmedialib_plugin.hpp>

#ifdef _WIN32
# ifdef VML_BATCH_EXPORT
#   define VML_BATCH_DECLSPEC  __declspec( dllexport )
# else
#   define VML_BATCH_DECLSPEC  __declspec( dllimport )
# endif
#else
# define VML_BATCH_DECLSPEC
#endif

namespace vmlbatch {

namespace ml = olib::openmedialib::ml;

struct task_exit
{
	int result;
};

struct task_create
{
	ml::store_type_ptr store;
};

typedef std::shared_ptr< ml::frame_type > frame_type_ptr;

struct task_display
{
	ml::store_type_ptr store;
	frame_type_ptr frame;
};

struct task_destroy
{
	ml::store_type *store;
};

VML_BATCH_DECLSPEC bool schedule( boost::any any );
VML_BATCH_DECLSPEC int process( );

}
